# @see https://techlife.cookpad.com/entry/2021/02/12/190000
require "tempfile"
require "open3"

class RollupProcessor
  ROLLUP_PATH = Shellwords.escape(Rails.root.join("node_modules/rollup/dist/bin/rollup").to_s)
  ROLLUP_CONFIG_PATH = Shellwords.escape(Rails.root.join("rollup.config.js").to_s)

  class Error < StandardError; end

  def render(context, empty_hash_wtf)
    self.class.run(@filename, @source, context)
  end

  def self.run(filename, source, context)
  end

  def call(input)
    data = input[:data]
    if has_rollup_pragma?(data)
      build(input[:data], input[:filename])
    else
      { data: data }
    end
  end

  def build(data, filename)
    dirname = File.dirname(filename)

    Tempfile.create("rollup_processor") do |temp|
      print("Rollup Process:" + filename + "\n")
      stdout, stderr, status = Open3.capture3(
        { "COLLECT_MODULE_PATHS" => temp.path },
        "#{ROLLUP_PATH} --config #{ROLLUP_CONFIG_PATH} -",
        stdin_data: data,
        chdir: dirname,
        )
      raise Error, "in #{filename}: #{stderr}" unless status == 0


      # Rollup に渡した JavaScript から `require()` や `import` で読み込まれたファイルのパスを集め、
      # それらのファイルが依存関係にあることを Rails に伝える。
      module_paths = JSON.parse(temp.read)
      dependencies = dependencies_from_paths(module_paths, dirname)

      { data: stdout, dependencies: dependencies }
    end
  end

  def dependencies_from_paths(paths, base_dir)
    node_modules_path = Rails.root.join("node_modules").to_s + "/"
    paths.reject do |path|
      path == "-" || path.start_with?("\0") || path.start_with?(node_modules_path)
    end.map do |path|
      realpath = File.realpath(path, base_dir)
      "file-digest://#{realpath}"
    end
  end

  def has_rollup_pragma?(data)
    %r{\/* @rollup-entry-point}x.match?(data)
  end
end