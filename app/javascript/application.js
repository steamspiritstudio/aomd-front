import("./nestfields/add")
import("./nestfields/remove")
import("./editform")
import("./result/ship/")
import("./result/chara/")

document.addEventListener("DOMContentLoaded", function () {
    document.body.getElementsByClassName("hamburger").item(0).addEventListener("click", function (e) {
        let el = this.parentElement;
        if (el.className.includes(" open")) {
            return el.className = el.className.replace(" open", "");
        }
        el.className += " open";
    });
});