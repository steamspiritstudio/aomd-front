interface Place {
    sector_id: number;
    x: number;
    y: number;
    station_id: number;
    name: string;
}

export interface ShipResultRecord {
    id: string,
    time: number,
    turn: number,
    type: string,
    to_place: Place,
    from_place: Place
}

export interface ResultApiResponse {
    ship_result: ShipResultRecord;
}

export default abstract class ResultComponents extends HTMLElement {
    private iconOnly: boolean;
    protected readonly shipName: string;

    public constructor(...params: any[]) {
        super()
        this.attachShadow({mode: "open"})
        this.iconOnly = this.getAttribute("icon-only") === "true";
        this.shipName = this.getAttribute('ship_name')
        this.render()
    }

    static get observedAttributes() {
        return ["icon-only"];
    }

    public attributeChangedCallback(name, oldValue, newValue) {
        if (name === "icon-only") {
            this.iconOnly = newValue === "true";
        }
        this.render()
    }

    abstract renderCore(): string;

    iconString(iconName: string) {
        return `<span class="game-icon ${iconName}" style="font-size: xx-large"></span>`
    }

    text(text: string) {
        if (this.iconOnly) {
            return '';
        }
        return `<span class="aomd-ship-action-text">` + (this.shipName ? `${this.shipName}は` : '') + `${text}</span>`;
    }

    fromPlaceString(): string {
        const place = JSON.parse(this.getAttribute("from_place"))
        return ResultComponents.placeString(place)
    }
    
    toPlaceString(): string {
        const place = JSON.parse(this.getAttribute("to_place"))
        return ResultComponents.placeString(place)
    }

    toPlacePoint() : string {
        const place = JSON.parse(this.getAttribute("to_place"))
        return `(${place.x}, ${place.y})`
    }
    
    private static placeString(place: Place) {
        return `${place.name}(${place.x}, ${place.y})`
    }

    render() {
        this.shadowRoot.innerHTML = `<div style="display: flex;align-items: self-start;">${this.renderCore()}</div>`
        Array.from(document.head.getElementsByTagName("link")).forEach(link => {
            if (link.getAttribute("rel") != "stylesheet" || !(link.href.includes("/assets/game-icons") || link.href.includes("/asset/game-icons"))) {
                return;
            }
            this.shadowRoot.append(link.cloneNode(true));
        })
    }
}