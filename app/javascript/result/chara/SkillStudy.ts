import ResultComponents from "result/component_base";

export default class SkillStudy extends ResultComponents {
    renderCore(): string {
        return this.iconString("game-icon-read") +
            this.text('模型組み立てのマニュアルを読んだ。')
    }
}

customElements.define('aomd-skillstudy', SkillStudy)