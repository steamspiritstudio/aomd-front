import ResultComponents from "result/component_base";

export default class RestCharacter extends ResultComponents {
    renderCore(): string {
        let json = JSON.parse(this.getAttribute("body_json")) as any;
        return this.iconString("game-icon-rocking-chair") +
            this.text(`休憩した。(HP:${json.hp}回復)`)
    }
}

customElements.define('aomd-rest', RestCharacter)