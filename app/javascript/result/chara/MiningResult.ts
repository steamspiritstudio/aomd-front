import ResultComponents from "result/component_base";

export default class MiningResult extends ResultComponents {
    renderCore(): string {
        return this.iconString("game-icon-battery-minus") +
            this.text('未実装の結果を引き当てた')
    }
}

customElements.define('aomd-mining', MiningResult)