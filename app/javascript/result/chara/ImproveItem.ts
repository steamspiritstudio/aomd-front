import ResultComponents from "result/component_base";

export default class ImproveItem extends ResultComponents {
    renderCore(): string {
        let json = JSON.parse(this.getAttribute("body_json")) as any;
        return this.iconString("game-icon-hammer-nails") +
            this.text(`模型組み立てを進めた。(進捗:${json.beforeQuality} => ${json.afterQuality})`)
    }
}

customElements.define('aomd-improveitem', ImproveItem)