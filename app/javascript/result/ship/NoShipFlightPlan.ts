import ResultComponents from "result/component_base";

export default class NoShipFlightPlan extends ResultComponents {
    renderCore(): string {
        return this.iconString("game-icon-anchor") +
            this.text(`${this.fromPlaceString()}で待機している。`)
    }
}

customElements.define('aomd-noshipflightplan', NoShipFlightPlan)