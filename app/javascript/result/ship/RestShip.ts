import ResultComponents from "result/component_base";

export default class RestShip extends ResultComponents {
    renderCore(): string {
        return this.iconString("game-icon-pocket-watch") +
            this.text(`${this.fromPlaceString()}で待機している。`)
    }
}

customElements.define('aomd-restship', RestShip)