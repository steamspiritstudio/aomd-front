import ResultComponents from "result/component_base";

export default class MovedSector extends ResultComponents {
    renderCore(): string {
        return this.iconString("game-icon-iron-hulled-warship") +
            this.text(`${this.fromPlaceString()}から${this.toPlaceString()}へ移動した。`)
    }
}

customElements.define('aomd-movedsector', MovedSector)