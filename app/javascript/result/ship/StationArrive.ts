import ResultComponents from "result/component_base";

export default class StationArrive extends ResultComponents {
    renderCore(): string {
        const place = JSON.parse(this.getAttribute("to_place"))
        return this.iconString("game-icon-harbor-dock") +
            this.text(`${place.name}に到着した。`)
    }
}

customElements.define('aomd-stationarrive', StationArrive)