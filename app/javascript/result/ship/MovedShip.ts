import ResultComponents from "result/component_base";

export default class MovedShip extends ResultComponents {
    renderCore(): string {
        return this.iconString("game-icon-ship-wheel") +
            this.text(`${this.fromPlaceString()}から${this.toPlacePoint()}へ移動した。`)
    }
}

customElements.define('aomd-movedship', MovedShip)