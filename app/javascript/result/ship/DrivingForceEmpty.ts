import ResultComponents from "result/component_base";

export default class DrivingForceEmpty extends ResultComponents {
    renderCore(): string {
        const shipName = this.shipName;
        return this.iconString("game-icon-battery-minus") +
            this.text(shipName ? '推進力の不足で動けなかった。' : '推進力不足')
    }
}

customElements.define('aomd-drivingforceempty', DrivingForceEmpty)