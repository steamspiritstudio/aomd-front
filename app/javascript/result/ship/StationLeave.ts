import ResultComponents from "result/component_base";

export default class StationLeave extends ResultComponents {
    renderCore(): string {
        const place = JSON.parse(this.getAttribute("from_place"))
        return this.iconString("game-icon-figurehead") +
            this.text(`${place.name}から出航した。`)
    }
}

customElements.define('aomd-stationleave', StationLeave)