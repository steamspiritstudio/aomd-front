import "../../assets/stylesheets/result.scss"
import MessageList from "../personal_plan/message.svelte";

window.addEventListener('DOMContentLoaded', function () {
    Array.from(document.getElementsByClassName("aomd-messages")).forEach((element) => {
        new MessageList({
            target: element,
            props: {
                messages: []
            }
        });
    });
});