/** @rollup-entry-point **/
import MessageList from './message.svelte'
import {createEmptyMessage} from "sss-telegraph/src/@type";

import PersonalActionPlan from "./personalActionPlan.svelte"
import "../../assets/stylesheets/personal_plan.scss"

function addChangedEvent() {
    // 航行計画フォームリアクティブ処理
    const shipActionPlanRoot = document.getElementsByClassName("aomd-ship-action-plan")[0];
    shipActionPlanRoot.addEventListener("input", function (e) {
        if (!e.target) {
            return;
        }
        var targetElement = e.target;
        if (targetElement.type === "range") {
            targetElement.previousElementSibling.innerHTML = ("00" + targetElement.value).slice(-2);
            return;
        }
        if (targetElement.tagName != "SELECT") {
            return;
        }
        const disabledPosition = !!targetElement.value.match("[^0]$");
        const moveFormRoot = targetElement.parentElement.parentElement.getElementsByClassName("aomd-move-to")[0];
        Array.from(moveFormRoot.getElementsByTagName("input")).forEach(function (input) {
            input.disabled = disabledPosition;
        });
    });
    // 航行計画フォーム初期設定
    Array.from(shipActionPlanRoot.getElementsByTagName("select")).forEach((selectEl) => {
        let event = new Event('input', {
            bubbles: true,
            cancelable: true,
        });
        selectEl.dispatchEvent(event);
    })
    Array.from(shipActionPlanRoot.getElementsByClassName("aomd-wait")).forEach((targetElement) => {
        targetElement.previousElementSibling.innerHTML = ("00" + targetElement.value).slice(-2);
    });

    // 船内活動計画初期化
    const componentRoot = document.getElementsByClassName('aomd-personal-plan-schedule');
    if (componentRoot.length <= 0) {
        return;
    }
    const shipId = componentRoot[0].getAttribute("data-ship_id");

    const data = JSON.parse(componentRoot[0].getAttribute("data-initial"));
    new PersonalActionPlan({
        target: componentRoot[0],
        props: {
            personalPlanList: data.action,
            shipId: shipId,
            shipResults: data.ship
        }
    });

    // 近距離通信
    Array.from(document.getElementsByClassName("aomd-telegraph-message")).forEach((element) => {
        const messages = JSON.parse(element.innerText);
        messages.push(createEmptyMessage());
        new MessageList({
            target: element,
            props: {
                messages
            },
            hydrate: true
        });
    });
}
window.addEventListener('DOMContentLoaded', addChangedEvent);