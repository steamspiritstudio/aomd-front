import type {ResultApiResponse, ShipResultRecord} from "result/component_base";

export const MAX_AP = 12;

export const PersonalPlanType = {
    Empty: "empty",
    Mining: "mining",
    Study: "study",
    Rest: "rest",
    Assembly: "assembly",
} as const;
export type PersonalPlanType = typeof PersonalPlanType[keyof typeof PersonalPlanType];

export type OnePlanJSON = PersonalPlan;

export type PersonalPlanJSON = {
    dno: number;
    character_id: number;
    name: string;
    plans: OnePlanJSON[];
}

interface PersonalPlan {
    planType: PersonalPlanType;
    useAp: number;
    fromTime: number;
}

export class EmptyPlan implements PersonalPlan {
    public readonly planType = PersonalPlanType.Empty;

    constructor(public useAp: number, public fromTime: number) {
    }
}

export class CommonPlan implements PersonalPlan {
    constructor(public readonly planType: PersonalPlanType, public useAp: number, public fromTime: number) {
    }
}

function convertPlan(planJson) {
    return new CommonPlan(planJson.planType, planJson.useAp, planJson.fromTime);
}

export type AomdApplyEvent = { detail: { personIndex: number, plan: CommonPlan, previousPlanType: PersonalPlanType } };

/**
 * @description 表示用に埋まっていない部分にEmptyPlanを挿入する
 */
function createPlanViewList(plans: PersonalPlan[]): PersonalPlan[] {
    let checkPlan = plans.concat([]).sort(function (a, b) {
        return a.fromTime - b.fromTime;
    });
    let lastViewList: PersonalPlan[] = [];
    for (let i = 0; i < MAX_AP;) {
        let plan = checkPlan.shift();
        if (!plan) {
            // 末尾
            let emptyAp = MAX_AP - i;
            for (let j = 0; j < emptyAp; j++) {
                lastViewList.push(new EmptyPlan(1, i + j));
            }
            break;
        }
        if (plan.fromTime == i) {
            lastViewList.push(plan);
            i += plan.useAp;
            continue;
        }
        let emptyAp = plan.fromTime - i;
        for (let j = 0; j < emptyAp; j++) {
            lastViewList.push(new EmptyPlan(1, i + j));
        }
        lastViewList.push(plan);
        i = plan.fromTime + plan.useAp;
    }
    return lastViewList;
}

export class PersonalPlansViewModel {
    constructor(public readonly character_id, public readonly dno: number, public readonly name: string, public readonly plans: PersonalPlan[]) {
    }

    static make(personalPlanJson: PersonalPlanJSON) {
        const plans = personalPlanJson.plans.map(convertPlan);
        return new PersonalPlansViewModel(personalPlanJson.character_id, personalPlanJson.dno, personalPlanJson.name, plans)
    }

    get viewPlans() {
        return createPlanViewList(this.plans);
    }

    updatePlan(updatePlan: CommonPlan) {
        const replaceIndex = this.plans.findIndex(function (plan) {
            return plan.fromTime === updatePlan.fromTime;
        });
        if (updatePlan.planType === PersonalPlanType.Empty) {
            if (replaceIndex === -1) {
                // 変化なし
                return;
            }
            this.plans.splice(replaceIndex, 1);
            return;
        }
        if (replaceIndex === -1) {
            this.plans.push(updatePlan);
            this.reorderPlan();
            return;
        }
        this.plans.splice(replaceIndex, 1, updatePlan);
        this.reorderPlan();
    }

    findPlan(fromTime: number): PersonalPlan | undefined {
        return this.plans.find((plan) => {
            return plan.fromTime <= fromTime && (plan.fromTime + plan.useAp) > fromTime;
        })
    }

    changeItem(fromTime: number, toTime: number): boolean {
        const fromPlan = this.findPlan(fromTime);
        const toPlan = this.findPlan(toTime);
        if (!fromPlan && !toPlan) {
            return false;
        }
        if (fromPlan) {
            fromPlan.fromTime = toTime;
        }
        if (!toPlan || fromPlan == toPlan) {
            this.reorderPlan();
            return true;
        }
        if (fromTime < toTime) {
            toPlan.fromTime -= toPlan.useAp - (toTime - toPlan.fromTime);
        } else {
            toPlan.fromTime += (fromPlan || {useAp: 1}).useAp + (toTime - toPlan.fromTime);
        }
        this.reorderPlan();
        return true;
    }

    reorderPlan() {
        let checkPlan = this.plans.concat([]).sort(function (a, b) {
            return a.fromTime - b.fromTime;
        });

        this.plans.length = 0;
        let currentFromTime = 0;
        checkPlan.forEach((plan) => {
            if (currentFromTime + plan.useAp > MAX_AP) {
                return;
            }
            this.plans.push(plan);
            if (plan.fromTime < currentFromTime) {
                plan.fromTime = currentFromTime;
            }
            currentFromTime = plan.fromTime + plan.useAp;
        });
    }
}


export function createShipResultTag(records: ShipResultRecord[], time: number, iconOnly: boolean): string {
    const targetRecords = records.filter((record) => record.time === time);
    return targetRecords.map((result, index) => {
        let tag = "<aomd-" + result.type + " ";
        Object.keys(result).forEach((key) => {
            if (key === "type") return;
            if (typeof result[key] === "object") {
                return tag += key + "=\"" + JSON.stringify(result[key]).replace(/[&'`"<>]/g, function (match) {
                    return {
                        '&': '&amp;',
                        "'": '&#x27;',
                        '`': '&#x60;',
                        '"': '&quot;',
                        '<': '&lt;',
                        '>': '&gt;',
                    }[match]
                }) + "\" ";
            }
            return tag += key + "=\"" + result[key] + "\" ";
        });
        if (iconOnly || index + 1 < targetRecords.length) {
            tag += "icon-only='true'"
        }
        return tag + "></aomd-" + result.type + "/>";
    }).join("\n")
}