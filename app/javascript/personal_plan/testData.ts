import {PersonalPlanJSON, PersonalPlanType} from "./personal_plan";
const testData: PersonalPlanJSON[] = [{
    dno: 9000,
    name: "HAL9000",
    character_id: 1,
    plans: [{
        planType: PersonalPlanType.Mining,
        useAp: 3,
        fromTime: 1
    }, {
        planType: PersonalPlanType.Study,
        useAp: 2,
        fromTime: 6
    }, {
        planType: PersonalPlanType.Mining,
        useAp: 1,
        fromTime: 9
    }]
}, {
    dno: 2001,
    name: "Bohman",
    character_id: 100,
    plans: [{
        planType: PersonalPlanType.Mining,
        useAp: 1,
        fromTime: 5
    }]
}, {
    dno: 8,
    name: "Alice",
    character_id: 100,
    plans: []
}, {
    dno: 3,
    name: "Bob",
    character_id: 100,
    plans: []
}, {
    dno: 20,
    name: "Ebi",
    character_id: 100,
    plans: []
}];

export default testData;