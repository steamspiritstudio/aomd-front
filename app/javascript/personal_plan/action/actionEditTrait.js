import EmptyApp from "./empty.svelte";
import MiningApp from "./mining.svelte";
import StudyApp from "./study.svelte";
import {PersonalPlanType} from "../personal_plan";
import AssemblyApp from "./assembly.svelte";
import RestApp from "./rest.svelte";

export function getActionComponent(type) {
    switch (type) {
        case PersonalPlanType.Study:
            return StudyApp;
        case PersonalPlanType.Mining:
            return MiningApp;
        case PersonalPlanType.Rest:
            return RestApp;
        case PersonalPlanType.Assembly:
            return AssemblyApp;
        case PersonalPlanType.Empty:
        default:
            return EmptyApp;
    }
}