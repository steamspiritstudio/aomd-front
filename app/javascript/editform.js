function addChangedEvent() {
    document.getElementById("main").addEventListener("change", function (e) {
        let card = e.target;
        while ((card = card.parentElement) && (!card.className || card.className.indexOf("aomd-card") === -1)) {
            if (card.className && card.className.indexOf("changed-card") > -1) {
                return;
            }
        }
        if (!(card && card.className)) {
            return;
        }
        let inputElement = e.target;
        let toggle = inputElement.defaultValue !== inputElement.value;
        let currentState = card.className.indexOf("changed-card") > -1;
        if (toggle === currentState) {
            return;
        }
        if (toggle) {
            card.className += " changed-card";
            return;
        }
        card.className = card.className.replace(" changed-card", "");
    });
}

window.addEventListener("DOMContentLoaded", addChangedEvent, {once: true})
window.addEventListener('DOMContentLoaded', addChangedEvent);

function submitEvent(e) {
    if(!e.target.form.checkValidity()){
        return false;
    }
    window.requestAnimationFrame(() => {
        e.target.disabled = true;
        e.target.setAttribute("uk-spinner", true);
    });
    return true;
}

window.addEventListener("DOMContentLoaded", function () {
    document.body.addEventListener("click", function (e) {
        const buttonEl = e.target;
        if (buttonEl.type !== "submit") {
            return;
        }
        submitEvent(e);
    })
});