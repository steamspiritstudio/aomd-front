class addFields {

    // This executes when the function is instantiated.
    constructor(){
        this.iterateLinks();
    }

    iterateLinks() {
        const self = this;
        document.getElementById('main').addEventListener("click", function(e) {
            // loop parent nodes from the target to the delegation node
            for (let target = e.target; target && target != this; target = target.parentNode) {
                if (target.matches(".add_fields")) {
                    self.handleClick.call(target, target, e);
                    break;
                }
            }
        }, false);
    }

    handleClick(link, e) {
        // Stop the function from executing if a link or event were not passed into the function.
        if (!link || !e) return;
        // Prevent the browser from following the URL.
        e.preventDefault();
        let maxLimit = parseInt(this.parentNode.getAttribute("data-aomd-nested-max"));
        if(maxLimit){
            let currentCount = Array.from(this.parentNode.children).reduce(function (count, node){
                if(!node.matches(".nested-fields")){
                    return count;
                }
                let destroyNode = node.querySelector('input[type="hidden"][name*="_destroy"]')
                if(destroyNode && destroyNode.value === "1"){
                    return count;
                }
                return count + 1;
            }, 0);
            if (maxLimit <= currentCount + 1){
                alert(maxLimit + "行まで追加できます。");
                return;
            }
        }

        // Save a unique timestamp to ensure the key of the associated array is unique.
        let time = new Date().getTime();
        // Save the data id attribute into a variable. This corresponds to `new_object.object_id`.
        let linkId = link.dataset.id;
        // Create a new regular expression needed to find any instance of the `new_object.object_id` used in the fields data attribute if there's a value in `linkId`.
        let regexp = linkId ? new RegExp(linkId, 'g') : null ;
        // Replace all instances of the `new_object.object_id` with `time`, and save markup into a variable if there's a value in `regexp`.
        let newFields = regexp ? link.dataset.fields.replace(regexp, time) : null ;
        // Add the new markup to the form if there are fields to add.
        newFields ? link.insertAdjacentHTML('beforebegin', newFields) : null ;
    }

}

// Wait for turbolinks to load, otherwise `document.querySelectorAll()` won't work
window.addEventListener('load', () => new addFields() );