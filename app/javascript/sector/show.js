import MapComponent from "aomd-map-module/src/AomdMap.svelte"

document.addEventListener("DOMContentLoaded", function () {
    const targetEl = document.getElementById("sectorMap");
    const attr = JSON.parse(targetEl.getAttribute("data-sector-map"));
    new MapComponent({
        target: targetEl, props: {
            sectorId: attr.sector_id,
            map: attr
        }
    });
});