namespace aomd.ship.components {
    export enum OWNER {
        SHIP,
        STATION
    }

    export type ComponentsJSON = {
        owner: OWNER,
        component_list: ComponentJson[],
    }

    export type ComponentJson = {
        id: string;
        ship_parts_id: string;
        name: string;
        manufacturer: Manufacturer;
        type: RawComponentType;
        price: number;
        hp: number;
        hp_max: number;
        mass: number;
        has_num: number,
        input_energy: number;
        output_energy: number;
        params: paramJSON[];
        cancelable: boolean;
        power_priority: number;
    }

    export const enum ShopFlag {
        none,
        Cancelable,
        Sell,
    }

    export type DisplayString = String;
    export type DisplayValueString = String;

    export type ComponentDisplay = {
        id: string,
        ship_parts_id: string,
        name: string,
        type: ComponentTypeString,
        manufacturer: Manufacturer,
        input_energy: DisplayValueString,
        mass: DisplayValueString,
        price: DisplayValueString,
        hp: DisplayValueString,
        cancelable: boolean, // まだ実行されていない取引はキャンセル可能
        sell: boolean, // 売却対象
        power_priority: number; // 電力優先順
    }

    export type RawComponentType = number;
    type ComponentTypeString = string;

    export namespace ComponentType {
        export const MAX_TYPE = 21;

        export function toName(type: RawComponentType): ComponentTypeString {
            switch (type) {
                case 1:
                    return "艦橋";
                case 2:
                    return "動力炉";
                case 3:
                    return "亜光速推進器";
                case 4:
                    return "超光速推進器";
                case 5:
                    return "貨物室";
                case 6:
                    return "船室";
                case 7:
                    return "通信設備";
                case 8:
                    return "生命維持装置";
                case 9:
                    return "採掘機";
                case 10:
                    return "医療室";
                case 11:
                    return "格納庫";
                case 12:
                    return "装甲";
                case 13:
                    return "空間偏向機";
                case 14:
                    return "亜空間電探";
                case 15:
                    return "自動消火装置";
                case 16:
                    return "研究室";
                case 17:
                    return "気閘室";
                case 18:
                    return "光線砲";
                case 19:
                    return "電磁投射砲";
                case 20: return "火砲";
                case 21: return "誘導弾発射装置";
            }
        }
    }

    export enum Manufacturer {
        IHI = 1,
        EDK = 2,
        EWR= 3,
        NGA = 4,
        LQT = 7,
        VeG = 8,
        ELD = 9
    }
    export namespace Manufacturer {
        export function toFullName(value: Manufacturer) {
            switch (value) {
                case Manufacturer.IHI:
                    return "帝国重工株式会社"
                case Manufacturer.EDK:
                    return "海老原電気興業"
                case Manufacturer.EWR:
                    return "東西和国鉄道"
                case Manufacturer.NGA:
                    return "夜行天研究団"
                case Manufacturer.LQT:
                    return "Le pitre qui marche Tartempion";
                case Manufacturer.VeG:
                    return "Venice Group"
                case Manufacturer.ELD:
                    return "帝国材木店"
            }
        }
        export function toShortName(value: Manufacturer) {
            switch (value) {
                case Manufacturer.IHI:
                    return "帝重"
                case Manufacturer.EDK:
                    return "海老電"
                case Manufacturer.EWR:
                    return "国鉄"
                case Manufacturer.NGA:
                    return "夜行研"
                case Manufacturer.LQT:
                    return "ラタレ";
                case Manufacturer.VeG:
                    return "ヴェニス"
                case Manufacturer.ELD:
                    return "帝店"
            }
        }
    }

    export enum ParameterType {
        Money,
        Mass,
        Hp,
        Propulsion,
        Fuel,
        Cargo,
        Mining,
        Shield,
        Energy,
    }
    export namespace ParameterType {
        export function getName(type: ParameterType) {
            switch (type){
                case ParameterType.Money:
                    return "価格";
                case ParameterType.Mass:
                    return "質量";
                case ParameterType.Hp:
                    return "HP";
                case ParameterType.Propulsion:
                    return "推進力";
                case ParameterType.Fuel:
                    return "燃料";
                case ParameterType.Cargo:
                    return "貨物";
                case ParameterType.Mining:
                    return "採掘";
                case ParameterType.Shield:
                    return "防御力場";
                case ParameterType.Energy:
                    return "エネルギー";
                default:
                    return "未知の力";
            }
        }
    }

    export type paramJSON = {
        type: ParameterType;
        value: number;
    }

    export type outputTotal = {
        propulsion: number,
        fuel: number,
        mass: number,
        shield: number
        cargo: number,
        input_energy: number,
        output_energy: number,
        mining: number,
        money: number
    }
}

export default aomd.ship.components;