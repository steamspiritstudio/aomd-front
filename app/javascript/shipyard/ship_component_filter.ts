import {ShipComponent} from "./ship_components";
import aomd from "./ship_component_enum";
import Manufacturer = aomd.Manufacturer;
import RawComponentType = aomd.RawComponentType;

interface FilterCondition {
    check(component: ShipComponent): boolean;
}

class ManufactureFilter implements FilterCondition {
    constructor(private readonly targetRawValues: Manufacturer[] = []) {
    }

    check(component: ShipComponent): boolean {
        return this.targetRawValues.length === 0 || this.targetRawValues.includes(component.manufacturer);
    }
}

class ComponentTypeFilter implements FilterCondition {
    constructor(private readonly targetRawValues: RawComponentType[] = []) {
    }

    check(component: ShipComponent): boolean {
        return this.targetRawValues.length === 0 || this.targetRawValues.includes(component.type);
    }
}

export class ShipComponentFilter implements FilterCondition {
    constructor(
        private readonly manufactureFilter: FilterCondition,
        private readonly componentTypeFilter: FilterCondition
    ) {
    }

    check(component: ShipComponent): boolean {
        return this.manufactureFilter.check(component) && this.componentTypeFilter.check(component);
    }

    static make(props?: { manufacture: Manufacturer[], component_type: RawComponentType[] }) {
        return new ShipComponentFilter(
            new ManufactureFilter(props?.manufacture),
            new ComponentTypeFilter(props?.component_type)
        );
    }
}