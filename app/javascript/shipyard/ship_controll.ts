import {ShipComponents} from "./ship_components";
import aomd from "./ship_component_enum";
import {SvelteComponent} from "svelte";
import ShipComponentList from "./ship_component_list.svelte";
import ShipParameter from "./ship_parameters.svelte";
import ComponentFilterBody from "./component_filter.svelte";
import {ShipComponentFilter} from "./ship_component_filter";
import ShipComponent from "shipyard/ship_component_enum";
import ShopFlag = aomd.ShopFlag;

function updateShipView(
    shipComponents: ShipComponents,
    shipComponentFilter: ShipComponentFilter,
    shipView: SvelteComponent,
    shipParameterView: SvelteComponent
) {
    shipView.$set({
        component_lists: {
            component_list: shipComponents.toDisplay(shipComponentFilter)
        }
    });
    shipParameterView.$set({
        params: shipComponents.toTotalParameter()
    });
}

function updateShipyardView(shipyardComponents: ShipComponents,
                            shipComponentFilter: ShipComponentFilter,
                            shipyardView: SvelteComponent
) {
    shipyardView.$set({
        component_lists: {
            component_list: shipyardComponents.toDisplay(shipComponentFilter)
        }
    });
}

function sendList(e) {
    if(!confirm("現在の設計で確定します。よろしいですか?")){
        e.stopImmediatePropagation();
        e.stopPropagation()
        return e.preventDefault();
    }
}

export function addChangedEvent() {
    let componentFilter = ShipComponentFilter.make();
    const yardTargetEl = document.getElementById("aomd-shipyard-list");
    const shipyardComponents = ShipComponents.make({
        owner: aomd.OWNER.STATION, component_list: JSON.parse(yardTargetEl.getAttribute("data-list"))
    });
    const shipyardView = new ShipComponentList({
        target: yardTargetEl,
        props: {
            component_lists: {
                owner: "Earth",
                component_list: shipyardComponents.toDisplay(componentFilter),
            },
            own: false
        }
    });
    shipyardView.$on("aomd-shop-action", function (e: CustomEvent) {
        // 購入
        const item = shipyardComponents.buy((e.detail.item as ShipComponent.ComponentDisplay).ship_parts_id, ShopFlag.Cancelable);
        shipComponents.add(item);
        updateShipView(shipComponents, componentFilter, shipView, shipParameterView);
    })

    const shipTargetEl = document.getElementById("aomd-ship-list");
    const shipComponents = ShipComponents.make({
        owner: aomd.OWNER.SHIP, component_list: JSON.parse(shipTargetEl.getAttribute("data-list"))
    });
    const shipView = new ShipComponentList({
        target: shipTargetEl,
        props: {
            component_lists: {
                owner: "D-1",
                component_list: shipComponents.toDisplay(componentFilter),
            },
            own: true
        }
    });
    shipView.$on("aomd-shop-action", function (e: CustomEvent) {
        if (e.detail.type === "buy") {
            return;
        }
        if (e.detail.type === "delete") {
            // 撤回
            shipComponents.cancel(e.detail.item.id);
        } else {
            // 売却
            shipComponents.sell(e.detail.item.id);
        }
        updateShipView(shipComponents, componentFilter, shipView, shipParameterView);
    })
    const shipParameterView = new ShipParameter({
        target: document.getElementById("aomd-status-list"),
        props: {
            params: shipComponents.toTotalParameter()
        }
    });

    // フィルター設定
    const filterView = new ComponentFilterBody({
        target: document.getElementById("aomd-filter-setting-panel"),
        props: {
            component_type: [],
            manufacture: []
        }
    });
    filterView.$on("aomd-filter-setting", function (e: CustomEvent) {
        componentFilter = ShipComponentFilter.make(e.detail);
        updateShipView(shipComponents, componentFilter, shipView, shipParameterView);
        updateShipyardView(shipyardComponents, componentFilter, shipyardView);
    })

    // 送信ボタン
    document.getElementById("sendShipYard").addEventListener("click", sendList);
}