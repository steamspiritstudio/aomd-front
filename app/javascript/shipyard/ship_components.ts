import ShipComponentEnum from "./ship_component_enum"
import aomd from "./ship_component_enum"
import {ShipComponentFilter} from "./ship_component_filter";
import ComponentsJSON = aomd.ComponentsJSON;
import ParameterType = aomd.ParameterType;
import ComponentType = aomd.ComponentType;
import ShopFlag = aomd.ShopFlag;

function getUniqueStr(myStrong?: number): string {
    let strong = 1000;
    if (myStrong) strong = myStrong;
    return (
        new Date().getTime().toString(16) +
        Math.floor(strong * Math.random()).toString(16)
    );
}

export class ShipComponents implements Iterable<ShipComponent> {
    constructor(
        private components: ShipComponent[]
    ) {
    }

    [Symbol.iterator](): Iterator<ShipComponent> {
        return this.components[Symbol.iterator]();
    }

    toDisplay(filter: ShipComponentFilter): ShipComponentEnum.ComponentDisplay[] {
        return this.components
            .filter(filter.check, filter)
            .map((component) => component.toDisplay());
    }

    toTotalParameter(): ShipComponentEnum.outputTotal {
        let moneyParameter = ShipParameters.makeEmpty();
        let sumParameter = ShipParameters.makeEmpty();
        this.components.forEach((component) => {
            if (component.id.indexOf("@") > 0) {
                moneyParameter = moneyParameter.add(component.parameters);
            }
            sumParameter = sumParameter.add(component.parameters)
        });
        return Object.assign(sumParameter.toDisplay(), {
            money: moneyParameter.get(ParameterType.Money).toDisplayValue()
        });
    }

    add(component: ShipComponent) {
        this.components.push(component);
    }

    delete(id: string): ShipComponent | null {
        const index = this.components.findIndex((component) => component.id === id);
        if (index === -1) {
            return null;
        }
        return this.components.splice(index, 1)[0];
    }

    buy(id: string, shopFlag: ShipComponentEnum.ShopFlag): ShipComponent {
        return this.findComponentByPartsId(id).clone(shopFlag);
    }

    public static make(componentsJSON: ComponentsJSON): ShipComponents {
        const components = componentsJSON.component_list.map(function (json) {
            return ShipComponent.make(json);
        });
        return new ShipComponents(components);
    }

    sell(id: string) {
        this.findComponent(id).shopFlag = ShopFlag.Sell;
    }

    cancel(id: string) {
        const component = this.findComponent(id);
        if (component.shopFlag === ShopFlag.Cancelable) {
            return this.delete(id);
        }
        component.shopFlag = ShopFlag.none;
    }

    private findComponent(id: string) {
        const component = this.components.find((component) => {
            return component.id === id;
        });
        if (!component) {
            throw "存在しないIDに対する操作:" + id;
        }
        return component;
    }

    private findComponentByPartsId(shipPartsId: string) {
        const component = this.components.find((component) => {
            return component.shipPartsId === shipPartsId;
        });
        if (!component) {
            throw "存在しないIDに対する操作:" + shipPartsId;
        }
        return component;
    }
}

export class ShipComponent {
    constructor(
        public readonly id: string,
        public readonly shipPartsId: string,
        public readonly type: ShipComponentEnum.RawComponentType,
        private name: string,
        public readonly manufacturer: ShipComponentEnum.Manufacturer,
        public readonly parameters: ShipParameters,
        public shopFlag: ShipComponentEnum.ShopFlag,
        public powerPriority: number
    ) {
        if (shopFlag == null) {
            this.shopFlag = ShipComponentEnum.ShopFlag.Cancelable;
        }
    }

    public static make(json: ShipComponentEnum.ComponentJson) {
        return new ShipComponent(
            json.id,
            json.ship_parts_id,
            json.type,
            json.name,
            json.manufacturer,
            ShipParameters.make(json),
            ShopFlag.none,
            json.power_priority
        );
    }

    public getParameter(type: ShipComponentEnum.ParameterType): Parameter {
        return this.parameters.get(type);
    }

    public toDisplay(): ShipComponentEnum.ComponentDisplay {
        return {
            id: this.id,
            ship_parts_id: this.shipPartsId,
            name: this.name,
            type: ComponentType.toName(this.type),
            mass: this.getParameter(ParameterType.Mass).toDisplayValue(),
            price: this.getParameter(ParameterType.Money).toDisplayValue(),
            manufacturer: this.manufacturer,
            input_energy: this.getParameter(ParameterType.Energy).toDisplayValue(),
            hp: this.getParameter(ParameterType.Hp).toDisplayValue(),
            cancelable: this.shopFlag !== ShopFlag.none,
            sell: this.shopFlag === ShopFlag.Sell,
            power_priority: this.powerPriority
        };
    }

    public clone(shopFlag: ShipComponentEnum.ShopFlag): ShipComponent {
        return new ShipComponent(
            this.shipPartsId + "@" + getUniqueStr(),
            this.shipPartsId,
            this.type,
            this.name,
            this.manufacturer,
            this.parameters,
            shopFlag,
            this.powerPriority
        );
    }
}

class ShipParameters {
    constructor(private parameters: Array<Parameter>) {
    }

    public add(other: ShipParameters): ShipParameters {
        let newParameters: Parameter[] = [].concat(this.parameters);
        other.parameters.forEach((otherParam) => {
            let paramIndex = newParameters.findIndex((param) => {
                return param.canAdd(otherParam);
            });
            if (paramIndex === -1) {
                newParameters.push(otherParam);
                return;
            }
            // 加算後の値で差し替え
            newParameters.splice(paramIndex, 1, newParameters[paramIndex].add(otherParam));
        });
        return new ShipParameters(newParameters);
    }

    public get(type: ParameterType) {
        return this.parameters.find((parameter) => {
                return parameter.equalsType(type);
            }) ||
            new NoneParameter(type);
    }

    [Symbol.iterator](): Iterator<Parameter> {
        return this.parameters[Symbol.iterator]();
    }

    static make(json: ShipComponentEnum.ComponentJson) {
        return new ShipParameters([
            new Energy(json.input_energy, json.output_energy),
            new BasicParameter(ShipComponentEnum.ParameterType.Money, json.price),
            new BasicParameter(ShipComponentEnum.ParameterType.Mass, json.mass),
            new ComponentsHp(json.hp, json.hp_max),
            ...json.params.map(BasicParameter.make)
        ]);
    }

    static makeEmpty() {
        return new ShipParameters([
            new Energy(0, 0),
            new BasicParameter(ShipComponentEnum.ParameterType.Money, 0),
            new BasicParameter(ShipComponentEnum.ParameterType.Mass, 0),
            new ComponentsHp(0, 0)
        ]);
    }

    public toDisplay(): ShipComponentEnum.outputTotal {
        return {
            cargo: this.get(ParameterType.Cargo).getValue(),
            fuel: this.get(ParameterType.Fuel).getValue(),
            input_energy: this.get(ParameterType.Energy).getValue(),
            output_energy: ((this.get(ParameterType.Energy)) as Energy).getMaxValue(),
            mining: this.get(ParameterType.Mining).getValue(),
            propulsion: this.get(ParameterType.Propulsion).getValue(),
            shield: this.get(ParameterType.Shield).getValue(),
            mass: this.get(ParameterType.Mass).getValue(),
            money: this.get(ParameterType.Money).getValue(),
        }
    }
}

interface Parameter {
    type: ParameterType;

    getName(): string;

    getValue(): number;

    add(other: Parameter): Parameter;

    toDisplay(): aomd.DisplayString;

    toDisplayValue(): aomd.DisplayValueString;

    validate(): boolean;

    equalsType(type: ParameterType): boolean;

    canAdd(param: Parameter): boolean;
}

class NoneParameter implements Parameter {
    constructor(public readonly type: ParameterType) {
    }

    add(other: Parameter): Parameter {
        return other;
    }

    equalsType(type: ParameterType): boolean {
        return this.type === type;
    }

    canAdd(param: Parameter): boolean {
        return this.equalsType(param.type);
    }

    getName(): string {
        return ShipComponentEnum.ParameterType.getName(this.type);
    }

    getValue(): number {
        return 0;
    }

    toDisplay(): aomd.DisplayString {
        return this.getName() + ": N/A";
    }

    toDisplayValue(): aomd.DisplayValueString {
        return this.getValue().toString();
    }

    validate(): boolean {
        return false;
    }
}

class BasicParameter implements Parameter {
    public readonly type: ShipComponentEnum.ParameterType;
    private readonly _value: number;

    constructor(type: ShipComponentEnum.ParameterType, value: number) {
        this.type = type;
        this._value = value;
    }

    add(other: Parameter): Parameter {
        if (!(other instanceof BasicParameter)) {
            return this;
        }
        if (this.type !== other.type) {
            return this;
        }
        return new BasicParameter(this.type, this._value + other._value);
    }

    equalsType(type: ParameterType): boolean {
        return this.type === type;
    }

    canAdd(param: Parameter): boolean {
        return this.equalsType(param.type);
    }

    getName(): string {
        return ShipComponentEnum.ParameterType.getName(this.type);
    }

    getValue(): number {
        return this._value;
    }

    toDisplay(): aomd.DisplayString {
        return this.getName() + ": N/A";
    }

    toDisplayValue(): aomd.DisplayValueString {
        return this.getValue().toString();
    }

    validate(): boolean {
        return false;
    }

    static make(json: ShipComponentEnum.paramJSON) {
        return new BasicParameter(json.type, json.value)
    }
}

abstract class LimitedParameter implements Parameter {
    public readonly abstract type: aomd.ParameterType;
    private readonly _addValue: number;
    private readonly _addMaxValue: number;

    constructor(addValue: number, addMaxValue: number) {
        this._addValue = addValue || 0;
        this._addMaxValue = addMaxValue || 0;
    }

    add(other: Parameter): Energy {
        if (!(other instanceof Energy)) {
            return this;
        }
        return new Energy(
            this._addValue + other._addValue,
            this._addMaxValue + other._addMaxValue
        );
    }

    getName(): string {
        return "エネルギー";
    }

    getValue(): number {
        return this._addValue;
    }

    getMaxValue(): number {
        return this._addMaxValue;
    }

    validate(): boolean {
        return this._addValue <= this._addMaxValue;
    }

    toDisplay(): aomd.DisplayString {
        return this.getName() + ": N/A";
    }

    toDisplayValue(): aomd.DisplayValueString {
        return this._addValue + "/" + this._addMaxValue;
    }

    equalsType(type: aomd.ParameterType) {
        return this.type === type;
    }

    canAdd(param: Parameter): boolean {
        return this.equalsType(param.type);
    }

}

class Energy extends LimitedParameter {
    public readonly type: ParameterType = ParameterType.Energy;
}

class ComponentsHp extends LimitedParameter {
    public readonly type: ParameterType = ParameterType.Hp;
}