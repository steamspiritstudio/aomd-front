# == Schema Information
#
# Table name: ship_action_results
#
#  id            :string           not null
#  body_json     :text             not null
#  ship_name     :string           not null
#  supply_power  :integer          not null
#  surplus_power :integer          not null
#  time          :integer          not null
#  turn          :integer          not null
#  x             :integer          not null
#  y             :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  sector_id     :bigint           not null
#  ship_id       :bigint           not null
#  station_id    :bigint
#
# Indexes
#
#  turn_for_ship_index   (turn,ship_id,time)
#  turn_in_sector_index  (turn,sector_id,time)
#
class ShipActionResult < ApplicationRecord
  def body_json
    if @body != nil
      return @body
    end
    @body = JSON.parse(self["body_json"])
  end

  def type
    self.body_json["type"]
  end

  def related_sector_ids
    body = self.body_json
    place = body["place"]
    unless place
      return [self["sector_id"]]
    end
    [self["sector_id"], place["sectorId"]]
  end

  def to_place(sectors)
    body = self.body_json
    place = body["place"]
    unless place
      return self.from_place(sectors)
    end
    res = {sector_id: place["sectorId"], x: place["x"], y: place["y"], station_id: place["stationId"]}
    res["name"] = self._get_place_name(res, sectors)
    res
  end

  def from_place(sectors)
    place = self
    res = {sector_id: place["sector_id"], x: place["x"], y: place["y"], station_id: place["station_id"]}
    res["name"] = self._get_place_name(res, sectors)
    res
  end

  def _get_place_name(place, sectors)
    sector = sectors.find {|r| r.id == place[:sector_id]}
    if place[:station_id]
      return sector.stations.find(place[:station_id]).name
    end
    sector.name
  end
end
