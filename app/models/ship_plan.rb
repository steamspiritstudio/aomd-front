# == Schema Information
#
# Table name: ship_plans
#
#  id                         :bigint           not null, primary key
#  hp(HP)                     :integer
#  hp_max(最大HP(保守))       :integer
#  priority(エネルギー優先度) :integer
#  tags(タグ)                 :text             default("{}")
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  part_id(パーツID)          :bigint
#  ship_id(船ID)              :bigint
#
class ShipPlan < ApplicationRecord
  extend ActiveHash::Associations::ActiveRecordExtensions
  belongs_to_active_hash :master_ship_parts, foreign_key: :part_id

  def to_shipyard_json
    self.master_ship_parts.to_shipyard_json
        .merge({
                 hp: self.hp,
                 hp_max: self.hp_max,
                 priority: self.priority,
                 id: self.id.to_s,
               })
  end

  def self.new_ship_parts(ship_id, part_id, priority)
    record = self.new(
      :ship_id => ship_id,
      :part_id => part_id,
      :priority => priority
    )
    master = record.master_ship_parts
    raise "指定された船パーツ(ID:" + part_id.to_s + ")はありません" if(master.nil?)
    record.hp_max = master[:hp_max]
    record.hp = master[:hp_max]

    return record
  end

end
