# == Schema Information
#
# Table name: transaction_plans
#
#  id         :bigint           not null, primary key
#  from_body  :text
#  from_type  :integer
#  to_body    :text
#  to_type    :integer
#  type       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  from_id    :integer
#  to_id      :integer
#
class TransactionPlan < ApplicationRecord
end
