# == Schema Information
#
# Table name: stations
#
#  id                    :bigint           not null, primary key
#  coordinate_x(座標X)   :integer          not null
#  coordinate_y(座標Y)   :integer          not null
#  name(名前)            :string
#  sector_id(セクターID) :bigint           not null
#
# Foreign Keys
#
#  fk_rails_...  (sector_id => sector_of_spaces.id)
#
class Station < ApplicationRecord
  belongs_to :sector_of_space, foreign_key: :sector_id
end
