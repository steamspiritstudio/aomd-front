# == Schema Information
#
# Table name: world_parameters
#
#  id         :bigint           not null, primary key
#  count      :integer
#  p_type     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_world_parameters_on_p_type  (p_type) UNIQUE
#
class WorldParameter < ApplicationRecord
  TYPE_TURN = "current_turn".freeze
end
