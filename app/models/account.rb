# == Schema Information
#
# Table name: accounts
#
#  id                  :bigint           not null, primary key
#  auth0_uid(認証情報) :string
#  name(ユーザー名)    :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
class Account < ApplicationRecord
  def self.from_token_payload(payload)
    unless payload
      return nil
    end
    find_by(auth0_uid: payload[:uid]) || create!(auth0_uid: payload[:uid], name: payload[:info][:nickname])
  end
end
