# == Schema Information
#
# Table name: sector_of_spaces
#
#  id               :bigint           not null, primary key
#  name(セクター名) :string           not null
#
class SectorOfSpace < ApplicationRecord
  has_many :sector_fairways, primary_key: :id, foreign_key: :sector_id_alpha
  has_many :stations, primary_key: :id, foreign_key: :sector_id
  has_many :terrain_generators, primary_key: :id, foreign_key: :sector_id
end
