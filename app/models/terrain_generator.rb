# == Schema Information
#
# Table name: terrain_generators
#
#  id                          :bigint           not null, primary key
#  central_coordinate_x(座標X) :integer          not null
#  central_coordinate_y(座標Y) :integer          not null
#  gas(ガス)                   :integer          default(0)
#  generate_type(生成タイプ)   :integer          default(1)
#  gravitation(重力)           :integer          default(0)
#  radial_rays(放射線)         :integer          default(0)
#  range(範囲)                 :integer          not null
#  small_body(小惑星)          :integer          default(0)
#  sector_id(セクターID)       :integer          not null
#
class TerrainGenerator < ApplicationRecord
  belongs_to :sector_of_space, primary_key: "sector_id", foreign_key: "id"
end
