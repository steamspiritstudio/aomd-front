# == Schema Information
#
# Table name: personal_plans
#
#  id                           :bigint           not null, primary key
#  daily(個人日誌)              :text
#  turn(ターン数)               :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  character_id(キャラクターID) :integer
#
# Indexes
#
#  index_personal_plans_on_character_id           (character_id)
#  index_personal_plans_on_turn_and_character_id  (turn,character_id) UNIQUE
#
class PersonalPlan < ApplicationRecord
  has_many :personal_plan_actions,
           lambda { |object|
             where(turn: object.turn)
           },
           dependent: :delete_all,
           primary_key: :character_id,
           foreign_key: :character_id

  has_many :ship_flight_schedules,
           dependent: :delete_all,
           primary_key: :character_id,
           foreign_key: :register_character_id

  accepts_nested_attributes_for :ship_flight_schedules,
                                reject_if: proc { |attributes| attributes["ship_id"].blank? ||
                                  ((
                                    attributes["to_station_id"].blank? ||
                                      attributes["to_station_id"].match(/0+/)
                                  ) &&
                                    (attributes["to_sector_id"].blank? ||
                                      attributes["coordinate_x"].blank? ||
                                      attributes["coordinate_y"].blank?
                                    )
                                  )
                                },
                                allow_destroy: true,
                                update_only: true

  def self.find_or_new(current_turn, character_id)
    @record = PersonalPlan.find_by({
                                     turn: current_turn,
                                     character_id: character_id
                                   })
    unless @record
      return new(current_turn, character_id)
    end

    @record
  end

  def self.new(current_turn, character_id)
    @record = super({
                      turn: current_turn,
                      character_id: character_id,
                      daily: ""
                    })
  end

  def move_to_place_id
    ::GameUniverse::PlaceId.new(self["move_to_sector_id"], self["move_to_station_id"])
  end

  def update_personal_action(json_hash)
    action_plan_json = json_hash.find do |record|
      record["character_id"] === self.character_id
    end
    return if action_plan_json == nil
    plans = ::PersonalAction::PersonalActionList.make_plans_for_json(action_plan_json["plans"])
    self.personal_plan_actions.delete_all
    plans.each_with_index do |plan, index|
      action_plan_record = PersonalPlanAction.make_from_plan(self.character_id, self.turn, index, plan)
      action_plan_record.save!
    end
  end
end
