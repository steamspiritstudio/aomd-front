# == Schema Information
#
# Table name: ship_flight_schedules
#
#  id                                          :bigint           not null, primary key
#  coordinate_x(移動先X)                       :integer          default(0)
#  coordinate_y(移動先Y)                       :integer          default(0)
#  wait_ap(滞在時間(0のみ特殊))                :integer          default(0), not null
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#  register_account_id(登録者ID)               :integer          not null
#  register_character_id(登録者キャラクターID) :integer          not null
#  ship_id(船ID)                               :integer          not null
#  to_sector_id(移動先宙域ID)                  :integer
#  to_station_id(移動先ステーションID)         :integer
#
# Indexes
#
#  ship_index  (ship_id,register_character_id)
#
class ShipFlightSchedule < ApplicationRecord
  def place
    self.to_sector_id.to_s + '-' + self.to_station_id.to_s
  end

  def place=(value)
    self.to_sector_id = 1
    self.to_station_id = 0
  end
end
