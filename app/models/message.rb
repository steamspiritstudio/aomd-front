# == Schema Information
#
# Table name: messages
#
#  id                                          :bigint           not null, primary key
#  body(通信本体)                              :text
#  has_type(送信先タイプ{1:船宛,2:セクター宛}) :integer
#  message_type(通信タイプ{1:低通信,2:高通信}) :integer
#  turn(受信ターン)                            :integer
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#  from_id(送信元船ID)                         :bigint
#  has_id(発信先ID)                            :bigint
#
class Message < ApplicationRecord
end
