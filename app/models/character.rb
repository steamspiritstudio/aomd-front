# == Schema Information
#
# Table name: characters
#
#  id(管理ID)                         :bigint           not null, primary key
#  callsign(コールサイン)             :string
#  helth(生命力)                      :integer
#  icon_ids(会話用アイコン)           :text
#  items(所持アイテム)                :text
#  money(所持金)                      :integer
#  name(名前)                         :string
#  personality(自由記入欄)            :text
#  place_id_in_ship(乗船している船ID) :integer
#  portrait(画像(立ち絵))             :string
#  register_turn(作成ターン)          :integer          not null
#  sp(残SP)                           :integer
#  tags(タグ)                         :text             default("{}")
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  account_id(アカウントID)           :bigint
#
class Character < ApplicationRecord
  mount_uploader :portrait, CharaPortraitUploader

  def personalityFree
    JSON.parse(personality)["free"] || ""
  end

  def ship_id
    # TODO: 複数人対応時に消す
    self["id"]
  end
end
