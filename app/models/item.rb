# == Schema Information
#
# Table name: items
#
#  id           :bigint           not null, primary key
#  has_count    :integer          unsigned, not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  character_id :integer          unsigned, not null
#  item_id      :integer          unsigned, not null
#
class Item < ApplicationRecord
  has_one :master_item, foreign_key: :id, primary_key: :item_id
end
