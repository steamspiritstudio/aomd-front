# == Schema Information
#
# Table name: sector_fairways
#
#  id                                     :bigint           not null, primary key
#  central_coordinate_alpha_x(α座標Y座標) :integer          not null
#  central_coordinate_alpha_y(α座標X座標) :integer          not null
#  central_coordinate_beta_x(β座標Y座標)  :integer          not null
#  central_coordinate_beta_y(β座標X座標)  :integer          not null
#  move_range_alpha(α座標移動ベクトル)    :integer          default(1), not null
#  move_range_beta(β座標移動ベクトル)     :integer          default(1), not null
#  sector_id_alpha(α座標セクターID)       :integer          not null
#  sector_id_beta(β座標セクターID)        :integer          not null
#
class SectorFairway < ApplicationRecord
  belongs_to :sector_of_space, primary_key: :id, foreign_key: :sector_id_alpha
  belongs_to :sector_of_space, primary_key: :id, foreign_key: :sector_id_beta
end
