# == Schema Information
#
# Table name: worlds
#
#  id         :bigint           not null, primary key
#  phase      :integer
#  turn       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class World < ApplicationRecord
end
