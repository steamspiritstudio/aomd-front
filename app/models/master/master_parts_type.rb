require "csv"

class MasterPartsType < ActiveCsv
  set_root_path "./db/master/"
  set_filename "master_parts_type"
end
