require "csv"

class MasterCorp < ActiveCsv
  set_root_path "./db/master/"
  set_filename "master_corp"
end
