class MasterShipParts < ActiveJSON::Base
  include ActiveHash::Associations
  set_root_path "./db/master/"
  set_filename "master_ship_parts"

  has_many :ship_parts
  belongs_to :master_corp, foreign_key: :corp
  belongs_to :master_parts_type, foreign_key: :type

  def to_shipyard_json
    params = []
    if self.type == 3 || self.type == 4
      params += [:type => 3, :value => self.com1]
    end
    if self.type == 5
      params += [:type => 5, :value => self.com1]
    end

    {
      "ship_parts_id": self.id,
      "name": self.name,
      "price": self.price,
      "type": self.type,
      "mass": self.mass,
      "hp": self.hp_max,
      "hp_max": self.hp_max,
      "input_energy": power < 0 ? -power : 0,
      "output_energy": power > 0 ? power : 0,
      "manufacturer": self.corp,
      "params": params
    }
  end
end