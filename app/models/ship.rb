# == Schema Information
#
# Table name: ships
#
#  id(管理ID)                                :bigint           not null, primary key
#  callsign(コールサイン)                    :string
#  cargo(船倉状況{[アイテムID],[量]})        :text
#  coordinate_x(現在位置_X座標)              :integer          not null
#  coordinate_y(現在位置_Y座標)              :integer          not null
#  free_txt(自由記入欄)                      :text
#  items(共有アイテム{[アイテムID],[数]})    :text
#  money(共有所持金)                         :bigint
#  name(船名)                                :string
#  picture(画像)                             :text
#  shelds(シールド残量)                      :integer
#  tags(タグ情報)                            :text             default("")
#  created_at                                :datetime         not null
#  updated_at                                :datetime         not null
#  captain_id(船長ID)                        :bigint
#  place_sector_id(現在位置_セクターID)      :bigint           not null
#  place_station_id(現在位置_ステーションID) :bigint
#
class Ship < ApplicationRecord
  has_many :ship_plans, foreign_key: :ship_id, primary_key: :id
  belongs_to :sector_of_space, primary_key: :id, foreign_key: :place_sector_id
  belongs_to :station, primary_key: :id, foreign_key: :place_station_id, optional: true

  def place_name
    if place_station_id != nil and place_station_id > 0
      return station.name
    end
    sector_of_space.name
  end

  def ship_parts_spec
    hp = 0
    hp_max = 0
    power = 0
    mass = 0

    # @type [ShipPlan] part
    self.ship_plans.each do |part|
      hp += part.hp
      hp_max += part.hp_max
      power += part.master_ship_parts.power
      mass += part.master_ship_parts.mass
    end

    [:hp => hp, :hp_max => hp_max, :power => power, :mass => mass]
  end
end
