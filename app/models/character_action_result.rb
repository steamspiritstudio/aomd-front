# == Schema Information
#
# Table name: character_action_results
#
#  id(ID(メッセージ用))             :string           not null, primary key
#  body_json(行動詳細)              :text             not null
#  ship_x(船のX座標)                :integer
#  ship_y(船のY座標)                :integer
#  time(発生時刻)                   :integer          not null
#  turn(発生ターン)                 :integer          not null
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  character_id(行動者)             :bigint           not null
#  sector_id(発生セクター)          :bigint           not null
#  ship_id(発生船)                  :bigint
#  station_id(ステーション着陸状態) :bigint
#
# Indexes
#
#  turn_by_character_index  (turn,character_id)
#  turn_by_sector_index     (turn,sector_id)
#  turn_by_ship_index       (turn,ship_id)
#
class CharacterActionResult < ApplicationRecord

  def body_json
    if @body != nil
      return @body
    end
    @body = JSON.parse(self["body_json"])
  end

  def type
    self.body_json["type"]
  end

  def related_sector_ids
    [sector_id]
  end
end
