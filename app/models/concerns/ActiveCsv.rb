class ActiveCsv < ActiveFile::Base
  include ActiveHash::Associations
  class << self
    def extension
      "csv"
    end

    def load_file
      CSV.open(full_path, 'r', headers: true).map do |row|
        row.to_hash
      end
    end
  end
end