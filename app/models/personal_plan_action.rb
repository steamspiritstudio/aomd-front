# == Schema Information
#
# Table name: personal_plan_actions
#
#  action_type(行動Type)        :string(32)
#  from_time(開始時間)          :integer
#  order(順番)                  :integer
#  param_json(補助パラメーター) :text
#  turn(ターン)                 :integer
#  use_ap(使用AP)               :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  character_id(キャラクターID) :integer
#  target_id(対象ID)            :integer
#
# Indexes
#
#  index_personal_plan_actions_on_turn_and_character_id_and_order  (turn,character_id,order) UNIQUE
#
class PersonalPlanAction < ApplicationRecord
  belongs_to :PersonalPlan,
             ->(o) { where "turn = ?", o.turn },
             foreign_key: "character_id",
             primary_key: "character_id"

  # @param [CommonPlan]
  def self.make_from_plan(character_id, turn, order, personal_plan)
    PersonalPlanAction.new(
      character_id: character_id,
      turn: turn,
      action_type: personal_plan.plan_type,
      use_ap: personal_plan.use_ap,
      from_time: personal_plan.from_time,
      target_id: 0,
      order: order
    )
  end

  def convert_plan
    ::PersonalAction::CommonPlan.new(self["action_type"], self["use_ap"], self["from_time"])
  end
end
