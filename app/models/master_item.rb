# == Schema Information
#
# Table name: master_items
#
#  id                         :bigint           not null, primary key
#  base_tag(タグ情報)         :text             default("{}"), not null
#  cargo_size(容量占有量)     :integer
#  max_use_action(最大使用量) :integer
#  name(アイテム名)           :string
#  tradable(取引可能か)       :boolean
#
class MasterItem < ApplicationRecord
  belongs_to :item
end
