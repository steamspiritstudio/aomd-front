class UniverseRepository
  include GameUniverse

  def self.find
    @all_stations = ::GameStation::Repository.all
    @all_sectors = SectorOfSpace.where.not(id: 0).all.includes(:terrain_generators).map do |sectorModel|
      SectorFactory.make(sectorModel, @all_stations.find_all { |station| station.place_id.sector_id == sectorModel["id"] })
    end
    @all_fairway = SectorFairway.all.map { |fairwayModel| SectorFairwayFactory.make_fairway(fairwayModel, @all_sectors) }
    GameUniverse::SectorFactory.set_fairways(@all_sectors, @all_fairway)
    Universe.new(@all_sectors, @all_stations)
  end
end

module GameUniverse
  class SectorFactory
    def self.make(sector_model, stations)
      terrain_generators = sector_model.terrain_generators.map do |model|
        ::GameSector::TerrainGenerator::Factory.make_from_model(model, sector_model)
      end
      GameSector::Sector.new(
        sector_model["id"],
        sector_model["name"],
        stations,
        GameSector::GridMap.new(25, 25, terrain_generators),
      )
    end

    def self.set_fairways(all_sectors, all_fairway)
      all_sectors.map do |sector|
        sector.set_fairways(make_fairways(sector, all_fairway))
      end
    end

    # @param [GameSector::Sector] sector
    # @param [GameFairway::Fairway[]] all_fairway
    def self.make_fairways(sector, all_fairway)
      @fairways = all_fairway.select do |fairway|
        fairway.include_sector_id(sector.id)
      end
      @fairways.prepend ::GameFairway::CircularFairway.new(::GameFairway::WayPoint.new(sector, ::ShipNavigation::WaypointArea.make_dummy))
      ::GameFairway::SectorFairways.new(@fairways)
    end
  end

  class SectorFairwayFactory
    def self.make_fairway(fairway_model, all_sectors)
      @sector_alpha = all_sectors.find { |sector| sector.id_equals(fairway_model["sector_id_alpha"]) }
      if @sector_alpha.nil?
        raise "No Sector/FairwayID=#{fairway_model['id']}-SectorId=#{fairway_model['sector_id_alpha']}"
      end

      @waypoint_alpha = ::GameFairway::WayPoint.new(
        @sector_alpha,
        ::ShipNavigation::WaypointArea.new(
          fairway_model["central_coordinate_alpha_x"],
          fairway_model["central_coordinate_alpha_y"],
          fairway_model["move_range_alpha"],
        ),
      )
      @sector_beta = all_sectors.find { |sector| sector.id_equals(fairway_model["sector_id_beta"]) }
      if @sector_beta.nil?
        raise "No Sector/FairwayID=#{fairway_model['id']}-SectorId=#{fairway_model['sector_id_beta']}"
      end

      @waypoint_beta = ::GameFairway::WayPoint.new(
        @sector_beta,
        ::ShipNavigation::WaypointArea.new(
          fairway_model["central_coordinate_beta_x"],
          fairway_model["central_coordinate_beta_y"],
          fairway_model["move_range_beta"],
        ),
      )
      ::GameFairway::Fairway.new(@waypoint_alpha, @waypoint_beta)
    end
  end
end
