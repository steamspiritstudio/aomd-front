import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

// loads the Icon plugin
UIkit.use(Icons);

document.addEventListener('load', function () {
    const tab01 = document.getElementById('tab01');
    if(!tab01){
        return;
    }
    UIkit.notification('UIkit initialized!');
    UIkit.tab(tab01, {});
    const tab02 = document.getElementById('tab02');
    if(tab02){
        UIkit.tab(tab02, {});
    }
})