module GameSector
  module TerrainGenerator
    class Generator
      def initialize(
        target_enumerable,
        terrain
      )
        @target_enumerable = target_enumerable
        @terrain = terrain
      end

      def each(proc)
        @target_enumerable.each(@terrain, &proc)
      end
    end

    class Factory
      # @param [::TerrainGenerator] terrain_model
      # @param [::SectorOfSpace] sector_model
      def self.make_from_model(terrain_model, sector_model)
        terrain = ::GameSector::Terrain.new(
          self._new_terrain_effect(terrain_model),
          self._new_terrain_flags(terrain_model)
        )
        target_enumerable = self._new_target_enumerable(terrain_model)
        Generator.new(target_enumerable, terrain)
      end

      # @param [::TerrainGenerators] terrain_model
      def self._new_target_enumerable(terrain_model)
        central_coordinate = ::ShipNavigation::Coordinate.new(terrain_model.central_coordinate_x, terrain_model.central_coordinate_y)
        range = terrain_model.range
        self.__new_target_enumerable(central_coordinate, range, terrain_model.generate_type)
      end

      # @param [::ShipNavigation::Coordinate] central_coordinate
      # @param [integer] range
      # @param [integer] generate_type
      def self.__new_target_enumerable(central_coordinate, range, generate_type)
        case generate_type
        when 1
          Rhombus.new(central_coordinate, range)
        when 2
          Square.new(central_coordinate, range)
        when 3
          Line.new(central_coordinate, range, true, false, 25)
        when 4
          Line.new(central_coordinate, range, false, true, 25)
        else
          Rhombus.new(central_coordinate, range)
        end
      end

      # @param [::TerrainGenerators] terrain_model
      def self._new_terrain_effect(terrain_model)
        ::GameSector::TerrainEffect::TerrainEffect.new(
          ::GameSector::TerrainEffect::RadialRays.new(terrain_model.radial_rays),
          ::GameSector::TerrainEffect::GasConcentration.new(terrain_model.gas),
          ::GameSector::TerrainEffect::SmallBody.new(terrain_model.small_body),
          # TODO: 引力の方角を実装
          ::GameSector::TerrainEffect::Gravitation.new(nil, terrain_model.gravitation),
        )
      end

      def self._new_terrain_flags(terrain_model)
        ::GameSector::TerrainFlags::TerrainFlags.new([])
      end

      # @param [integer] src_sector_id
      # @param [::GameFairway::Fairway[]] sector_fairways
      def self.make_from_fairways(src_sector_id, sector_fairways)
        sector_fairways.map do |fairway|
          terrain = ::GameSector::Terrain.new(
            ::GameSector::TerrainEffect::TerrainEffect.new_normalize,
            ::GameSector::TerrainFlags::TerrainFlags.new([
                                                           ::GameSector::TerrainFlags::Fairway.new(src_sector_id, fairway)
                                                         ])
          )
          waypoint_area = fairway.get_src_waypoint(src_sector_id).waypoint_area
          target_enumerable = self.__new_target_enumerable(waypoint_area, waypoint_area.range, 1)
          Generator.new(target_enumerable, terrain)
        end
      end
    end

    # type1: 菱形展開
    class Rhombus
      # @param [::ShipNavigation::Coordinate] center_coordinate
      # @param [Number] range
      def initialize(center_coordinate, range)
        @center_coordinate = center_coordinate
        @range = range
      end

      def each(terrain)
        radius = @range
        current_coordinate = @center_coordinate

        radius.downto(0) do |max_y|
          diff_x = radius - max_y
          max_y.downto(0) do |y|
            yield terrain, current_coordinate.renew_apply_diff(::ShipNavigation::MoveDiff.new(diff_x, y))
            if diff_x.positive?
              yield terrain, current_coordinate.renew_apply_diff(::ShipNavigation::MoveDiff.new(-diff_x, -y))
            end
            if y != 0
              yield terrain, current_coordinate.renew_apply_diff(::ShipNavigation::MoveDiff.new(diff_x, -y))
            end
            if (y != 0) && diff_x.positive?
              yield terrain, current_coordinate.renew_apply_diff(::ShipNavigation::MoveDiff.new(-diff_x, y))
            end
          end
        end
      end
    end

    # type2: 四角形展開
    class Square
      # @param [::ShipNavigation::Coordinate] center_coordinate
      # @param [Number] range
      def initialize(center_coordinate, range)
        @center_coordinate = center_coordinate
        @range = range
      end

      def each(terrain)
        radius = @range
        current_coordinate = @center_coordinate

        (current_coordinate.x - radius).upto(current_coordinate.x + radius) do |x|
          (current_coordinate.y - radius).upto(current_coordinate.y + radius) do |y|
            yield terrain, ::ShipNavigation::Coordinate.new(x, y)
          end
        end
      end
    end

    class Line
      # @param [::ShipNavigation::Coordinate] center_coordinate
      # @param [Number] range
      # @param [Boolean] x_line
      # @param [Boolean] y_line
      # @param [Number] max
      def initialize(center_coordinate, range, x_line, y_line, max)
        @center_coordinate = center_coordinate
        @range = range
        @x_line = x_line
        @y_line = y_line
        @max = max
      end

      def each(terrain)
        @range.downto(0) do |diff|
          if @x_line
            @max.times do |x|
              yield terrain, ::ShipNavigation::Coordinate.new(x, @center_coordinate.y + diff)
              yield terrain, ::ShipNavigation::Coordinate.new(x, @center_coordinate.y - diff) if diff != 0
            end
          end
          next unless @y_line

          @max.times do |y|
            yield terrain, ::ShipNavigation::Coordinate.new(@center_coordinate.x + diff, y)
            yield terrain, ::ShipNavigation::Coordinate.new(@center_coordinate.x - diff, y) if diff != 0
          end
        end
      end
    end
  end
end
