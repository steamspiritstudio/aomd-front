module GameSector
  module TerrainEffect
    class TerrainEffect
      attr_reader :radial_rays, :gas, :small_body, :attraction

      # @param [TerrainEffect::RadialRays] radial_rays
      # @param [TerrainEffect::GasConcentration] gas
      # @param [TerrainEffect::SmallBody] small_body
      # @param [TerrainEffect::Gravitation] gravitation
      def initialize(radial_rays, gas, small_body, gravitation)
        @radial_rays = radial_rays
        @gas = gas
        @small_body = small_body
        @attraction = gravitation
      end

      def interstellar?
        @radial_rays.empty? ||
          @gas.empty? ||
          @small_body.empty?
      end

      # @param [TerrainEffect] other
      def renew_with_add(other)
        TerrainEffect.new(
          @radial_rays.add(other.radial_rays),
          @gas.add(other.gas),
          @small_body.add(other.small_body),
          @attraction.add(other.attraction),
        )
      end

      def self.new_normalize
        TerrainEffect.new(
          RadialRays.new_normalize,
          GasConcentration.new_normalize,
          SmallBody.new_normalize,
          Gravitation.new_normalize,
        )
      end
    end

    module TerrainInteger
      attr_reader :value
      protected :value

      def initialize(value)
        @value = value
      end

      def add(other_value)
        self.class.new(@value + other_value.value)
      end

      def empty?
        @value.zero?
      end

      def to_i
        @value
      end

      def to_s
        @value.to_s
      end
    end

    # 放射線
    class RadialRays
      include TerrainInteger

      # @return [self]
      def self.new_normalize
        new(0)
      end
    end

    # ガス濃度
    class GasConcentration
      include TerrainInteger

      # @return [self]
      def self.new_normalize
        new(0)
      end
    end

    # 小天体
    class SmallBody
      include TerrainInteger

      # @return [self]
      def self.new_normalize
        new(0)
      end
    end

    # 引力
    class Gravitation
      attr_reader :direction, :power
      protected :direction, :power

      # TODO: 実装
      def initialize(direction, power)
        @direction = direction
        @power = power
      end

      def add(other)
        Gravitation.new(
          @direction,
          #        @direction + other.direction,
          @power + other.power,
        )
      end

      # @return [Gravitation]
      def self.new_normalize
        new(0, 0)
      end
    end
  end
end
