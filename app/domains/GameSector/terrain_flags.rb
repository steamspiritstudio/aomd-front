module GameSector::TerrainFlags
  class TerrainFlags
    attr_reader :flags
    protected :flags
    extend Forwardable
    def_delegators :@flags, :size, :find_all, :push, :freeze, :empty?

    def initialize(flags)
      @flags = flags
    end

    def renew_with_concat(other)
      return self if other.empty?
      TerrainFlags.new(@flags + other.flags)
    end

    def self.new_empty
      TerrainFlags.new([])
    end
  end

  class Fairway
    # @param [integer] src_sector_id
    # @param [::ShipNavigation::GameFairway] fairway
    def initialize(src_sector_id, fairway)
      @src_sector_id = src_sector_id
      @fairway = fairway
    end
  end
end
