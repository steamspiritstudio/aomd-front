module GameSector
  class ViewMap
    # @param [integer] max_x
    # @param [integer] max_y
    # @param [Terrain[][]] map
    # @param [Array<GameShip>] all_ship_in_sector
    # @param [Array<GameStation>] all_station_in_sector
    def initialize(max_x, max_y, map, all_ship_in_sector, all_station_in_sector)
      @max_x = max_x
      @max_y = max_y
      @map = map
      @all_ship_in_sector = all_ship_in_sector
      @all_station_in_sector = all_station_in_sector
    end

    def get_cell(coordinate)
      if coordinate.x > @max_x
        return Terrain.make_interstellar
      end
      if coordinate.y > @max_y
        return Terrain.make_interstellar
      end

      @map[coordinate.x][coordinate.y]
    end

    def get_all
      result = []
      @max_x.times do |x|
        result_y = []
        @max_y.times do |y|
          coordinate = ::ShipNavigation::Coordinate.new(x, y)
          grid_cell = get_cell(coordinate)
          ships = @all_ship_in_sector.filter do |ship|
            ship.current_place.coordinate.equals(coordinate)
          end
          stations = @all_station_in_sector.filter do |station|
            station.coordinate.equals(coordinate)
          end
          result_y << ViewCell.new(grid_cell, ships, stations)
        end
        result << result_y
      end
      result
    end
  end

  class ViewCell
    # @param [Terrain] grid_cell
    # @param [::GameShip::Ship[]] ships
    # @param [::GameStation::Station[]] stations
    def initialize(grid_cell, ships, stations)
      @grid_cell = grid_cell
      @ships = ships
      @stations = stations
    end

    # @return [string]
    def to_mark_string
      return "☆" if @stations.count.positive?

      unless @grid_cell.interstellar?
        terrain_effect = @grid_cell.terrain_effect
        return "※" if terrain_effect.radial_rays.to_i.positive?
        return "∴" if terrain_effect.gas.to_i.positive?

        return "＃"
      end
      return "○" if @ships.count.positive?

      fairway_flags = @grid_cell.terrain_flags.find_all do |flags|
        flags.instance_of?(::GameSector::TerrainFlags::Fairway)
      end
      return "門" unless fairway_flags.empty?

      "□"
    end
  end
end
