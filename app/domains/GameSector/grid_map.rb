module GameSector
  class GridMap
    attr_reader :max_x, :max_y

    # @param [integer] max_x
    # @param [integer] max_y
    # @param [Array<TerrainGenerator>] terrain_generators
    def initialize(max_x, max_y, terrain_generators)
      @max_x = max_x
      @max_y = max_y
      @terrain_generators = terrain_generators
      # 地形発生源を適用するまではすべて通常空間
      @map = max_y.times.map do |_y|
        max_x.times.map do |_x|
          Terrain.make_interstellar
        end
      end
    end

    # @param [integer] sector_id
    # @param [SectorFairway[]] sector_fairways
    def apply_terrain(sector_id, sector_fairways)
      rewrite_proc = proc do |terrain, coordinate|
        if __is_out_space(coordinate)
          return
        end

        @map[coordinate.x][coordinate.y] =
          @map[coordinate.x][coordinate.y].renew_with_add(terrain)
      end
      @terrain_generators.each do |generator|
        generator.each(rewrite_proc)
      end
      TerrainGenerator::Factory.make_from_fairways(sector_id, sector_fairways).each do |generator|
        generator.each(rewrite_proc)
      end
    end

    def __is_out_space(coordinate)
      if (coordinate.x >= @max_x) || coordinate.x.negative?
        return true
      end
      if (coordinate.y >= @max_y) || coordinate.y.negative?
        return true
      end

      false
    end

    def get_cell(coordinate)
      if __is_out_space(coordinate)
        return Terrain.make_interstellar
      end

      @map[coordinate.x][coordinate.y]
    end

    # @param [Cell] cell
    # @param [::ShipNavigation::MoveDiff] move_diff
    def get_move_cell(cell, move_diff)
      get_cell(::ShipNavigation::Coordinate.new(cell.x + move_diff.diffX, cell.y + move_diff.diffY))
    end

    def convert_to_search_map
      SearchGridMap.new(@max_x, @max_y, @map)
    end

    def convert_view_map(ships_in_sector, sector_stations)
      ViewMap.new(@max_x, @max_y, @map, ships_in_sector, sector_stations)
    end
  end

  class SearchGridMap
    # @param [integer] max_x
    # @param [integer] max_y
    # @param [Array<Array<Terrain>>] map
    def initialize(max_x, max_y, map)
      @map = max_y.times.map do |y|
        max_x.times.map do |x|
          SearchCell.new(x, y, map[x][y], max_x * max_y + 1)
        end
      end
    end

    # @param [Coordinate] coordinate
    def get_cell(coordinate)
      unless (coordinate.x >= 0) && (coordinate.x < max_x)
        return SearchCell.make_unmovable_cell(coordinate)
      end

      @map[coordinate.x][coordinate.y] or SearchCell.make_unmovable_cell(coordinate)
    end
  end

  class SearchCell
    attr_reader :x, :y, :move_cost_from_start

    def initialize(x, y, terrain, search_cost)
      @x = x
      @y = y
      @terrain = terrain
      @move_cost_from_start = search_cost
      @active = false
    end

    def set_move_cost(prev_search_cost)
      # 現在のところ、全地形は移動コスト1
      @move_cost_from_start = prev_search_cost + 1
      @active = true
    end

    def is_active
      @active
    end

    def equal(other)
      @x == other.x and @y == other.y
    end

    def self.make_unmovable_cell(coordinate)
      cell = SearchCell.new(coordinate.x, coordinate.y, Terrain.make_interstellar, 2**31 - 1)
      cell.set_move_cost(cell.move_cost_from_start)
      cell
    end

    def range
      1
    end

    def to_json(*_args)
      {
        x: @x,
        y: @y,
        move_cost_from_start: @move_cost_from_start
      }
    end
  end
end
