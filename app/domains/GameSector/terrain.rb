module GameSector
  class Terrain
    @@interstellar_cell = nil
    attr_reader :terrain_effect, :terrain_flags

    # @param [TerrainEffect::TerrainEffect] terrain_effect
    # @param [TerrainFlags::TerrainFlags] terrain_flags
    def initialize(terrain_effect, terrain_flags)
      @terrain_effect = terrain_effect
      @terrain_flags = terrain_flags
    end

    def interstellar?
      @terrain_effect.interstellar?
    end

    # @param [Terrain] other
    def renew_with_add(other)
      Terrain.new(
        @terrain_effect.renew_with_add(other.terrain_effect),
        @terrain_flags.renew_with_concat(other.terrain_flags)
      )
    end

    # 何の影響もないセルを返す
    # @return [Terrain]
    def self.make_interstellar
      @@interstellar_cell or (@@interstellar_cell = Terrain.new(
        TerrainEffect::TerrainEffect.new_normalize,
        TerrainFlags::TerrainFlags.new_empty
      ))
    end
  end
end
