module PersonalAction
  class PersonalActionList
    attr_reader :character_id, :dno, :name, :plans

    def initialize(character_id, dno, name, plans)
      @character_id = character_id
      @dno = dno
      @name = name
      @plans = plans
    end

    # @param [Array<Hash>] plans_hash_array
    def self.make_plans_for_json(plans_hash_array)
      plans_hash_array.map do |record|
        CommonPlan.new(record["planType"], record["useAp"], record["fromTime"])
      end
    end

    def as_json(*)
      return [
        "character_id": @character_id,
        "dno" => @dno,
        "name" => @name,
        "plans" => @plans
      ]
    end

    def to_json(*)
      as_json.to_json
    end
  end
end
