module PersonalAction
  class CommonPlan
    attr_reader :plan_type, :use_ap, :from_time

    def initialize(plan_type, use_ap, from_time)
      @plan_type = plan_type
      @use_ap = use_ap
      @from_time = from_time
    end

    def target_id
      0
    end

    def as_json(*)
      {
        :planType => @plan_type,
        :useAp => @use_ap,
        :fromTime => @from_time
      }
    end

    def to_json(*)
      as_json.to_json
    end
  end
end