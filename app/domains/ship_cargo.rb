class ShipCargo
  # TODO: 積荷の実態もここで管理する
  # @param [integer] cargo_mass
  def initialize(cargo_mass, cargo_max_mass)
    @cargo_mass = cargo_mass
  end

  def total_cargo_mass
    @cargo_mass
  end
end
