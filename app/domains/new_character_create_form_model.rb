class NewCharacterCreateFormModel
  include ActiveModel::Model

  attr_accessor :name, :portrait, :personalityFree, :ship_name
  validates :name, :ship_name, presence: true

  def save(account)
    return false if invalid?

    character = ::Character::create(
      account_id: account.id,
      name: name,
      personality: JSON.dump({:free => personalityFree}),
      icon_ids: "{}",
      money: 0,
      helth: 100,
      sp: 0,
      items: "{}",
      place_id_in_ship: 1,
      register_turn: GameHelper.current_turn
    )
    character["callsign"] = "D-" + character["id"].to_s
    character.save
    if portrait
      character.portrait.store!(portrait)
    end
    game_ship = ::GameShip::Factory.make_new(character["id"], ship_name, ::SectorOfSpace.find(AoMD::Application.config.x.start_place[:sector_id]))
    ::GameShip::Repository.persist(game_ship)

    return character["id"]
  end
end