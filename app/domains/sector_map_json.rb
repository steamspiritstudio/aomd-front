module SectorMapJson
  class SectorMapJson
    attr_reader :sector_id, :ships, :stations, :fairway

    # @param [::GameSector::GameSector] game_sector
    # @param [Array<::GameShip::Ship>] ships
    def initialize(game_sector, ships)
      @sector_id = game_sector.id
      @ships = ships.map do |ship|
        Struct::Ship.new(
          :id => ship.id,
          :callsign => ship.callsign,
          :name => ship.name,
          :coordinate_x => ship.current_place.coordinate.x,
          :coordinate_y => ship.current_place.coordinate.y,
        )
      end
      # @param [::GameStation::Station]
      @stations = game_sector.sector_stations.map do |station|
        Struct::Station.new(
          :id => station.place_id.station_id,
          :name => station.name,
          :coordinate_x => station.coordinate.x,
          :coordinate_y => station.coordinate.y
        )
      end
      @fairway = game_sector.sector_fairways.map do |fairway|
        waypoint = fairway.get_dest_waypoint(game_sector.id)
        waypoint_src = fairway.get_src_waypoint(game_sector.id)
        Struct::Fairway.new(
          :central_coordinate_alpha_x => waypoint.waypoint_area.x,
          :central_coordinate_alpha_y => waypoint.waypoint_area.y,
          :move_range_alpha => waypoint.waypoint_area.range,
          :sector_id_alpha => waypoint.sector.id,
          :sector_name_alpha => waypoint.sector.name,

          :central_coordinate_beta_x => waypoint_src.waypoint_area.x,
          :central_coordinate_beta_y => waypoint_src.waypoint_area.y,
          :move_range_beta => waypoint_src.waypoint_area.range,
        )
      end
      .filter do |fairwayStruct|
        # 自己ループ航路を除外
        fairwayStruct.move_range_alpha >= 0
      end
    end
  end

  Struct.new("Ship", :id, :callsign, :coordinate_x, :coordinate_y, :name, :picture, keyword_init: true)

  Struct.new("Station", :id, :name, :coordinate_x, :coordinate_y, keyword_init: true)

  Struct.new("Fairway",
             :central_coordinate_alpha_x,
             :central_coordinate_alpha_y,
             :central_coordinate_beta_x,
             :central_coordinate_beta_y,
             :move_range_alpha,
             :move_range_beta,
             :sector_id_alpha,
             :sector_name_alpha,
             keyword_init: true
  )
end



