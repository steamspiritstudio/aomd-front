module GameCharacter
  class PlayerCharacter
    attr_reader :character_id, :callsign, :name, :register_turn, :ship_id

    # @param [CharacterId] character_id
    # @param [integer] ship_id
    # @param [string] callsign
    # @param [string] name
    # @param [integer[]] item_ids
    # @param [Money] money
    # @param [GameItem::Item[]] items
    def initialize(
      character_id,
      ship_id,
      callsign,
      name,
      icon_ids,
      money,
      items,
      register_turn
    )
      @character_id = character_id
      @ship_id = ship_id
      @callsign = callsign
      @name = name
      @icon_ids = icon_ids
      @money = money
      @items = items
      @register_turn = register_turn
    end
  end

  class CharacterId
    PLAYER = 0

    attr_reader :id, :type

    def initialize(type, id)
      @type = type
      @id = id
    end
  end

  class Money
    def initialize(money)
      @money = money
    end

    def to_i
      @money
    end

    delegate :to_s, to: :to_i
  end

  class Repository
    # @param [CharacterId] character_id
    def self.find(character_id)
      Factory.make ::Character.find(character_id.id)
    end

    def self.find_player(raw_character_id)
      find CharacterId.new(CharacterId::PLAYER, raw_character_id)
    end

    # @return [Array<Character>]
    def self.all
      ::Character.all.map do |model|
        Factory.make(model)
      end
    end
  end

  class Factory
    # @param [::Character] character_model
    def self.make(character_model)
      PlayerCharacter.new(
        CharacterId.new(CharacterId::PLAYER, character_model.id),
        character_model.id,
        character_model.callsign,
        character_model.name,
        character_model.icon_ids,
        Money.new(character_model.money),
        character_model.items,
        character_model.register_turn,
      )
    end
  end
end
