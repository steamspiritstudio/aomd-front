module GameShip
  class Ship
    attr_reader :id, :callsign, :name, :current_place, :flight_management, :ship_parts_spec

    # @param [integer] id
    # @param [string] name
    # @param [OwnerShip] ownership
    # @param [Object] ship_parts_spec
    # @param [Engine::NormalEngine] engine
    # @param [ShipCargo] cargo
    # @param [::ShipNavigation::NormalFms] flight_management
    # @param [CurrentPlace] current_place
    def initialize(
      id,
      callsign,
      name,
      ownership,
      ship_parts_spec,
      engine,
      cargo,
      flight_management,
      current_place
    )
      @id = id
      @callsign = callsign
      @name = name
      @ownership = ownership
      @current_place = current_place
      @engine = engine
      @cargo = cargo
      @flight_management = flight_management
      @ship_parts_spec = ship_parts_spec
    end

    # @param [Array<::ShipNavigation::MoveDiff>] routes
    # @return [CurrentPlace] current_place
    def do_move_in_sector(routes)
      arrival_cell = @engine.do_ignition(routes, @cargo)
      @current_place = @current_place.renew_in_sector(arrival_cell)
    end

    def change_current_sector(fairway)
      # MEMO: ステーションにいたままセクターが変わることはない
      dest_fairway_point = fairway.get_dest_waypoint(@current_place.place_id.sector_id)
      dest_sector = dest_fairway_point.sector
      @current_place = CurrentPlace.make(
        dest_sector.id,
        CurrentPlace::OUTSIDE_STATION,
        dest_sector.name,
        dest_fairway_point.waypoint_area.x,
        dest_fairway_point.waypoint_area.y,
      )
    end

    # ステーション到着
    # @param [GameStation::Station] station
    def arrive_station(station)
      @current_place = @current_place.renew_arrive_station(station)
    end

    def get_captain_id
      @ownership.character_id
    end

    # 船章HTML
    # @return String
    def ship_emblem_html
      '<span style="transform: scale(5);margin:50px" uk-icon="icon: lifesaver"></span>'.html_safe
    end
  end

  class Ownership
    attr_reader :character_id

    def initialize(character_id)
      @character_id = character_id
    end
  end

  class Repository
    # @todo 船のIDを正しく発行できるように発行ロジックを考える

    def self.find(ship_id)
      # 今は船IDと船長IDは同じ
      find_or_new(ship_id)
    end

    def self.find_by_character_id(character_id)
      # 今は船IDと船長IDと乗員のキャラクターIDは同じ
      find_or_new(character_id.id)
    end

    def self.find_or_new(captain_id)
      ship_record = ::Ship.eager_load(:ship_plans).find_by(captain_id: captain_id)
      if ship_record.nil?
        sector_record = ::SectorOfSpace.find(AoMD::Application.config.x.start_place[:sector_id])
        return Factory.make_new(captain_id, "ニフリー号", sector_record)
      end
      sector_record = ::SectorOfSpace.find(ship_record["place_sector_id"])
      items = ItemRepository.get_items(captain_id)
      Factory.make_from_model(ship_record, sector_record, items)
    end

    def self.persist(ship)
      ship_record = ::Ship.find_or_initialize_by({
                                                   id: ship.get_captain_id,
                                                   captain_id: ship.get_captain_id
                                                 })
      ship_record["callsign"] = "D-#{ship_record['captain_id'].to_s.rjust(4, '0')}"
      ship_record["place_sector_id"] = ship.current_place.sector_id
      ship_record["place_station_id"] = ship.current_place.station_id
      ship_record["coordinate_x"] = ship.current_place.coordinate.x
      ship_record["coordinate_y"] = ship.current_place.coordinate.y
      ship_record["name"] = ship.name
      # 未実装な項目達
      ship_record["shelds"] = 0
      ship_record["money"] = 0
      ship_record["cargo"] = '[]'
      ship_record["items"] = '[]'
      ship_record["free_txt"] = ''

      ship_record.save!
    end

    def self.get_in_sector(sector_id)
      ::Ship.where(place_sector_id: sector_id).select(:id).map do |ship_id|
        find(ship_id)
      end
    end
  end

  class Factory
    def self.make_new(captain_id, name, sector_record)
      initialPlace = AoMD::Application.config.x.start_place
      current_place = CurrentPlace.make(
        sector_record["id"],
        CurrentPlace::OUTSIDE_STATION,
        sector_record["name"],
        initialPlace[:x],
        initialPlace[:y],
      )
      make(0,
           "D-" + captain_id.to_s,
           name,
           captain_id,
           current_place,
           "{}",
           "{}",
           [],
           [],
           [])
    end

    # @param [::Ship] ship
    def self.make_from_model(ship, sector_record, items)
      name = sector_record["name"]
      station_id = ship["place_station_id"].to_i
      # TODO: 表示上の仕様が入り込んでいるのでやめたい
      if station_id.positive?
        station = sector_record.stations.find station_id
        name += " - " + station.name
      end
      current_place = CurrentPlace.make(
        sector_record["id"],
        station_id,
        name,
        ship["coordinate_x"],
        ship["coordinate_y"],
      )
      ship_cargos = items.map(&:cargo)

      make(0, ship["callsign"] || "D-" + ship["id"], ship["name"] || "ニフリー号", ship["captain_id"], current_place, "{}", "{}", ship_cargos, [], ship.ship_parts_spec)
    end

    def self.make(
      ship_type,
      callsign,
      name,
      captain_id,
      current_place,
      tags,
      facilities,
      ship_cargos,
      icon_ids,
      ship_parts_spec
    )
      ownership = Ownership.new(captain_id)
      total_cargo_mass = ship_cargos.sum(&:mass)
      cargo = ShipCargo.new(total_cargo_mass, 530000)
      engine = ::GameShip::Engine::NormalEngine.new
      fms = ::ShipNavigation::NormalFms.new
      GameShip::Ship.new(
        ownership.character_id,
        callsign,
        name,
        ownership,
        ship_parts_spec,
        engine,
        cargo,
        fms,
        current_place,
      )
    end
  end

  class CurrentPlace
    OUTSIDE_STATION = nil

    attr_reader :place_name, :place_id, :coordinate

    # @param [integer] place_id
    # @param [string] place_name
    # @param [::ShipNavigation::Coordinate]
    def initialize(place_id, place_name, coordinate)
      @place_id = place_id
      @place_name = place_name
      @coordinate = coordinate
    end

    def get_name
      @place_name
    end

    def station_id
      @place_id.station_id
    end

    def sector_id
      @place_id.sector_id
    end

    def in_station?
      @place_id.station_id != OUTSIDE_STATION
    end

    # @param [GameSector::SearchCell] move_to_cell
    def renew_in_sector(move_to_cell)
      place_id = ::GameUniverse::PlaceId.new(@place_id.sector_id, CurrentPlace::OUTSIDE_STATION)
      ship_coordinate = ::ShipNavigation::Coordinate.new(move_to_cell.x, move_to_cell.y)
      GameShip::CurrentPlace.new(place_id, @place_name, ship_coordinate)
    end

    def renew_arrive_station(station)
      GameShip::CurrentPlace.new(station.place_id, station.name, station.coordinate)
    end

    def self.make(sector_id, station_id, place_name, coordinate_x, coordinate_y)
      place_id = ::GameUniverse::PlaceId.new(sector_id, station_id || CurrentPlace::OUTSIDE_STATION)
      ship_coordinate = ::ShipNavigation::Coordinate.new(coordinate_x, coordinate_y)
      GameShip::CurrentPlace.new(place_id, place_name, ship_coordinate)
    end
  end
end
