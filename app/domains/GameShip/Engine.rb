module GameShip
  module Engine
    class NormalEngine
      attr_reader :power, :power_offset

      protected :power, :power_offset
      def initialize
        @power = 530000
        @power_offset = 0
        @used_movement = 0
      end

      # 与えられたルートに従って進めるだけ進む
      # @param [Array<::ShipNavigation::MoveDiff>] route
      # @param [ShipCargo] ship_cargo
      # @return [::GameSector::SearchCell]
      def do_ignition(route, ship_cargo)
        residual_movement =
          power + power_offset - ship_cargo.total_cargo_mass
        - @used_movement
        arrival_cell = route.max_by do |cell|
          # @param [::GameSector::SearchCell] cell
          if cell.move_cost_from_start < residual_movement
            cell.move_cost_from_start
          else
            -1
          end
        end
        @used_movement += arrival_cell.move_cost_from_start
        arrival_cell
      end
    end

    class ComplexEngine
      # @param [NormalEngine[]] engines
      def initialize(engines, power_offset)
        @engines = engines
        @power_offset = power_offset
      end

      protected

        def power
          @engines.reduce { |engine, n| engine.power + n } + @power_offset
        end
        attr_reader :power_offset
    end
  end
end
