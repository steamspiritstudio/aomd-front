module GameStation
  class Station
    attr_reader :place_id, :name, :coordinate

    # @param [PlaceId] place_id
    # @param [string] name
    # @param [::ShipNavigation::Coordinate] coordinate
    def initialize(place_id, name, coordinate)
      @place_id = place_id
      @name = name
      @coordinate = coordinate
    end

    def current_sector_id
      @place_id.sector_id
    end

    def new_dest_station_view_model
      GameFairway::DestSectorViewModel.new(@place_id, @name)
    end
  end

  class Repository
    def self.all
      # テーブル追加がpushされたら修正する
      ::Station.all.map do |record|
        _new(record)
      end
    end

    # @param [StationId] station_id
    def self.find(id)
      _new(::Station.find(id))
    end

    def self._new(station_record)
      coordinate = ::ShipNavigation::Coordinate.new(
        station_record["coordinate_x"],
        station_record["coordinate_y"],
      )
      Station.new(
        ::GameUniverse::PlaceId.new(station_record["sector_id"], station_record["id"]),
        station_record["name"],
        coordinate,
      )
    end

    def self.get_from_sector_id(raw_sector_id)
      ::Station.where({ sector_id: raw_sector_id }).map do |d|
        _new(d)
      end
    end
  end
end
