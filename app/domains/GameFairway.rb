module GameFairway
  class SectorFairways
    extend Forwardable
    def_delegators :@fairways_array, :size, :map, :empty?
    # @param [GameFairway::Fairway[]] fairways_array
    def initialize(fairways_array)
      @fairways_array = fairways_array
    end

    # @param [::GameShip::CurrentPlace] ship_current_place
    def can_move_to_place_views(ship_current_place)
      MoveToPlaceList.new(@fairways_array.map do |fairway|
        fairway.get_dest_sector_view_model(ship_current_place.sector_id)
      end
        .flatten, ship_current_place)
    end

    # @return [GameFairway::Fairway]
    def get_fairway(sector_id)
      @fairways_array.find { |n| n.include_sector_id(sector_id) }
    end
  end

  class Fairway
    # @param [Waypoint] waypoint_alpha
    # @param [Waypoint] waypoint_beta
    def initialize(waypoint_alpha, waypoint_beta)
      @alpha = waypoint_alpha
      @beta = waypoint_beta
    end

    def include_sector_id(sector_id)
      @alpha.sector.id_equals(sector_id) || @beta.sector.id_equals(sector_id)
    end

    # @return [WayPoint]
    def get_src_waypoint(sector_id)
      if @alpha.sector.id_equals(sector_id)
        return @alpha
      end

      @beta
    end

    def get_dest_waypoint(from_sector_id)
      if @alpha.sector.id_equals(from_sector_id)
        return @beta
      end

      @alpha
    end

    def get_dest_sector(from_sector_id)
      get_dest_waypoint(from_sector_id).sector
    end

    def get_dest_sector_view_model(form_sector_id)
      dest_sector = get_dest_sector(form_sector_id)
      list = [DestSectorViewModel.new(dest_sector.place_id, dest_sector.name)]
      list.concat(dest_sector.sector_stations.map(&:new_dest_station_view_model))
    end
  end

  class WayPoint
    attr_reader :sector, :waypoint_area

    # @param [GameSector::Sector] sector
    # @param [::ShipNavigate::WaypointArea] waypoint_area
    def initialize(sector, waypoint_area)
      @sector = sector
      @waypoint_area = waypoint_area
    end
  end

  # その場に留まる(=移動しない)航路。移動先選択用のクラスで、経路探索には利用しない。
  class CircularFairway < Fairway
    def initialize(current_sector)
      super(current_sector, current_sector)
    end
  end

  # 移動先の指定画面構築用のViewModel
  class DestSectorViewModel
    attr_reader :place_id, :view_name

    def initialize(place_id, view_name)
      @place_id = place_id
      @view_name = view_name
    end

    def masked(ship_current_place)
      unless @place_id.equal(ship_current_place.place_id)
        return self
      end

      DestSectorViewModel.new(@place_id, "移動なし/ #{@view_name}")
    end
  end

  class MoveToPlaceList
    include Enumerable

    def initialize(view_models, ship_current_place)
      @view_models = view_models
      @ship_current_place = ship_current_place
    end

    def each
      @view_models.each do |m|
        yield m.masked(@ship_current_place)
      end
    end
  end
end
