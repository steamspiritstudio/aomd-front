module GameUniverse
  class Universe
    # @param [GameSector::Sector[]]
    def initialize(all_sectors, all_stations)
      @all_sectors = all_sectors
      @all_stations = all_stations
    end

    def find_sector(sector_id)
      @all_sectors.find { |n| n.id_equals(sector_id) }
    end

    def find_station(sector_id, station_id)
      if !station_id.nil? && station_id.positive?
        target_station_id = ::GameUniverse::PlaceId.new(sector_id, station_id)
        return @all_stations.find { |station| station.place_id.equal(target_station_id) }
      end
      find_sector(sector_id).sector_stations
    end

    def get_reported_sector
      @all_sectors
    end
  end

  class PlaceId
    attr_reader :sector_id, :station_id

    def initialize(raw_sector_id, raw_station_id)
      @sector_id = raw_sector_id
      @station_id = raw_station_id
    end

    # @param [PlaceId] place_id
    def equal(place_id)
      place_id.sector_id == @sector_id and place_id.station_id == @station_id
    end

    def to_s
      "#{@sector_id}-#{@station_id}"
    end

    def in_station?
      @station_id != ::GameShip::CurrentPlace::OUTSIDE_STATION
    end

    def self.new_from_s(raw_string_id)
      id_array = raw_string_id.split("-")
      PlaceId.new(id_array[0].to_i, id_array[1].to_i)
    end
  end
end
