module ShipNavigation
  class NormalFms
    # @param [GridMap] grid_map
    # @param [Coordinate] coordinate_from
    # @param [Coordinate] coordinate_to
    def make_route(grid_map, coordinate_from, coordinate_to)
      search_grid_map = grid_map.convert_to_search_map
      start = search_grid_map.get_cell(coordinate_from)
      start.set_move_cost(0)
      # マッピング開始
      stack_array = [start]
      while stack_array.count.positive?
        if NormalFms._recursive_coordinate(search_grid_map, stack_array, coordinate_to, stack_array.shift)
          break
        end
      end
      # 目標地点から逆に辿る
      goal = search_grid_map.get_cell(coordinate_to)
      current_cell = goal
      route = [goal]
      until current_cell.equal(start)
        # 一番コスト値が小さい場所を探す
        next_move_diff = DIRECTION.min_by do |_key, move_diff|
          search_grid_map.get_move_cell(current_cell, move_diff).move_cost_from_start
        end[1]
        next_cell = search_grid_map.get_move_cell(current_cell, next_move_diff)
        route.push(next_cell)
        current_cell = next_cell
      end
      route.reverse!
    end

    # @param [SearchGripMap] search_grid_map
    # @param [SearchCell] current_cell
    def self._recursive_coordinate(search_grid_map, stack_array, coordinate_to, current_cell)
      DIRECTION.each do |_key, move_diff|
        next_cell = search_grid_map.get_move_cell(current_cell, move_diff)
        if next_cell.nil? || next_cell.is_active
          next
        end
        if next_cell.move_cost_from_start <= current_cell.move_cost_from_start
          next
        end

        next_cell.set_move_cost(current_cell.move_cost_from_start)
        stack_array.push next_cell
        if coordinate_to.equals(next_cell)
          # 到達目標を見つけたので打ち切り
          return true
        end
      end
      false
    end
  end

  DIRECTION = {
    RIGHT: MoveDiff.new(0, 1),
    LEFT: MoveDiff.new(0, -1),
    TOP: MoveDiff.new(1, 0),
    BOTTOM: MoveDiff.new(-1, 0),
    RIGHT_TOP: MoveDiff.new(1, 1),
    RIGHT_BOTTOM: MoveDiff.new(-1, 1),
    LEFT_TOP: MoveDiff.new(1, -1),
    LEFT_BOTTOM: MoveDiff.new(-1, -1)
  }.freeze
end
