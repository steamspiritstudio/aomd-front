module ShipNavigation
  class Coordinate
    attr_reader :x, :y

    # @param [integer] x
    # @param [integer] y
    def initialize(x, y)
      @x = x
      @y = y
    end

    # @param [MoveDiff] move_diff
    def renew_apply_diff(move_diff)
      Coordinate.new(x + move_diff.diffX, y + move_diff.diffY)
    end

    def contains_coordinate(coordinate)
      equals(coordinate)
    end

    # @param [Coordinate] coordinate
    def equals(coordinate)
      coordinate.x == @x and coordinate.y == @y
    end

    def to_s
      "#{@x} - #{@y}"
    end
  end

  class WaypointArea < Coordinate
    attr_reader :x, :y, :range

    # @param [integer] x
    # @param [integer] y
    # @param [integer] range
    def initialize(x, y, range)
      super(x, y)
      @range = range
    end

    def equals(waypoint_area)
      super(waypoint_area) and waypoint_area.range == @range
    end

    def contains_coordinate(coordinate)
      (coordinate.x - @x).abs <= range and (coordinate.y - @y).abs <= range
    end

    def self.make_dummy
      WaypointArea.new(0, 0, -1)
    end
  end

  class MoveDiff
    attr_reader :diffX, :diffY

    # @param [-1,0,1] diffX
    # @param [-1,0,1] diffY
    def initialize(diffX, diffY)
      @diffX = diffX
      @diffY = diffY
    end
  end
end
