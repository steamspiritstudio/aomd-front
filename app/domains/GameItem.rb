module GameItem
  class Item
    attr_reader :name, :cargo

    def initialize(name, type, cargo, ownership)
      @name = name
      @type = type
      @cargo = cargo
      @ownership = ownership
    end

    def has_count
      @cargo.has_count
    end
  end

  class Type
    def initialize(type, tradable, tags, max_use_action)
      @tags = tags
    end
  end

  class Cargo
    attr_reader :has_count

    def initialize(size, has_count)
      @size = size
      @has_count = has_count
    end

    # @return [Integer]
    def mass
      @size * @has_count
    end
  end

  class OwnerShip
    def initialize(character_id) end
  end
end
