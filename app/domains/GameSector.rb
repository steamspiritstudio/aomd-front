module GameSector
  class Sector
    attr_reader :id, :name, :sector_fairways, :sector_stations, :grid_map

    def initialize(id, name, sector_stations, grid_map)
      @id = id
      @name = name
      @sector_stations = sector_stations
      @grid_map = grid_map
      @applied_terrain = false
    end

    def id_equals(sector_id)
      @id == sector_id
    end

    def place_id
      ::GameUniverse::PlaceId.new(@id, 0)
    end

    # @param [SectorFairway::SectorFairways] sector_fairways
    def set_fairways(sector_fairways)
      unless @sector_fairways.nil?
        raise "論理エラー:航路情報が二回代入されました"
      end

      @sector_fairways = sector_fairways
    end

    def apply_terrain
      if @applied_terrain
        return
      end
      @grid_map.apply_terrain(@id, @sector_fairways)
      @applied_terrain = true
    end

    def convert_view_map(ships_in_sector)
      grid_map.convert_view_map(ships_in_sector, @sector_stations)
    end
  end
end
