class PersonalPlanController < ApplicationController
  include Secured

  def index
    @personal_plan = current_character_plan(current_character_record)
    @current_ship = GameShip::Repository.find_or_new(current_character_raw_id)
    @current_sector = UniverseRepository.find.find_sector(@current_ship.current_place.sector_id)
    @can_move_dest_places = @current_sector.sector_fairways.can_move_to_place_views(@current_ship.current_place)

    @ship_flight_schedules = current_ship_flight_schedules(current_character_raw_id, current_character)
    if @ship_flight_schedules.empty?
      @ship_flight_schedules = @personal_plan.ship_flight_schedules.build
    end

    plan_actions = @personal_plan.personal_plan_actions.map do |record|
      record.convert_plan
    end
    @personal_plan_actions = ::PersonalAction::PersonalActionList.new(current_character_raw_id, current_character_record["callsign"], current_character_record["name"], plan_actions)

    @ship_action_results = ShipActionResult.where(:ship_id => @current_ship.id, :turn => GameHelper.current_turn)
    related_sector_ids = @ship_action_results.flat_map { |record| record.related_sector_ids }.uniq
    @sectors = SectorOfSpace.eager_load(:stations).where(id: related_sector_ids)
    @ship_action_results = @ship_action_results.map do |record|
      body = record.body_json
      output = record.slice(:id, :ship_id, :turn, :time).merge(:type => body["type"], :from_place => record.from_place(@sectors))
      if body["place"]
        output[:to_place] = record.to_place(@sectors)
      end
      output
    end
  end

  def edit
    character_record = current_character_record
    @current_ship = GameShip::Repository.find_or_new(current_character_raw_id)
    @plan = current_character_plan(character_record)
    if @plan.new_record?
      @plan.save!
    end
    update_param = params
                     .require(:personal_plan)
                     .permit(
                       :daily,
                       ship_flight_schedules_attributes: %i[place coordinate_x coordinate_y wait_ap _destroy id]
                     )
    update_param[:ship_flight_schedules_attributes].each { |entity|
      record = entity[1]
      record["ship_id"] = @current_ship.id
      match = record["place"].match(/(\d+)-(\d+)/)
      record["to_sector_id"] = match[1]
      record["to_station_id"] = match[2] || 0
      record["register_account_id"] = current_user.id
    } if update_param[:ship_flight_schedules_attributes] != nil
    @plan.update!(update_param)
    if params["personal_plan_json"]
      @plan.update_personal_action(JSON.parse! params["personal_plan_json"])
    end
    @plan.save!
    redirect_to action: "index"
  end

  # @return [PersonalPlan]
  def current_character_plan(current_character)
    PersonalPlan.find_or_new(Object::GameHelper.current_turn, current_character.id)
  end

  def current_ship_flight_schedules(ship_id, current_character)
    ::ShipFlightSchedule.where(
      register_character_id: current_character.character_id.id,
      ship_id: ship_id
    )
  end
end