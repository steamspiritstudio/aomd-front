class ResultController < ApplicationController
  def index
    current_turn = GameHelper.current_turn
    @characters = GameCharacter::Repository.all.filter do |character|
      character.register_turn != current_turn
    end
    @turn = current_turn
  end

  def show
    target_turn = GameHelper.current_turn - 1
    @turn = target_turn
    @character = GameCharacter::Repository.find_player(params["id"])
    if @character.register_turn > target_turn
      respond_to do |format|
        format.html { render "result/new_comer" }
        format.json {
          response = { @ship_action_results => [] }
          render response, :handlers => :jbuilder
        }
      end
      return
    end

    character_id = @character.character_id
    @ship = GameShip::Repository.find_by_character_id(character_id)
    @ship_action_results = ShipActionResult.where(:ship_id => @ship.id, :turn => target_turn)
    related_sector_ids = @ship_action_results.flat_map { |record| record.related_sector_ids }.uniq
    @sectors = SectorOfSpace.eager_load(:stations).where(id: related_sector_ids)
    @character_action_results = CharacterActionResult.where(:character_id => character_id, :turn => target_turn)
    @plan = PersonalPlan.where({ character_id: character_id, turn: target_turn }).first

    @max_ap = 12 # TODO: どこかで一律に設定できるファイルを置きたい　マスタデータがいいかな?

    respond_to do |format|
      format.html
      format.json { render :formats => :json, :handlers => :jbuilder }
    end
  end
end
