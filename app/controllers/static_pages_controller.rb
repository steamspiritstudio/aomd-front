class StaticPagesController < ApplicationController
  def credit
    fresh_when(template: 'static_pages/credits', public: true)
  end
end
