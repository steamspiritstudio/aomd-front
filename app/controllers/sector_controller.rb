class SectorController < ApplicationController
  def index
    universe = UniverseRepository.find
    @ship_in_sector = Ship::group(:place_sector_id).count
    @ship_in_sector.default = 0
    @all_reported_sector = universe.get_reported_sector
  end

  def show
    sector_id = params["id"].to_i
    # TODO: 公開情報かのチェックを入れる
    @universe = UniverseRepository.find
    @sector = @universe.find_sector(sector_id)
    render_404 and return if @sector.nil?

    all_ship_in_sector = ::GameShip::Repository.get_in_sector(sector_id)
    @sector.apply_terrain

    @sector_map_json = ::SectorMapJson::SectorMapJson.new(@sector, all_ship_in_sector)
  end
end
