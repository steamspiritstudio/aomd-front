class ShipDetailController < ApplicationController
  def index
    @ships = Ship.includes(:station, :sector_of_space).all
  end

  def show
    @ship = Ship.find(params[:id])
    @characters = [Character.find(params[:id])] # 複数人対応時に修正
  end
end
