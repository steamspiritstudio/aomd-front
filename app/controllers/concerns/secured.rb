module Secured
  extend ActiveSupport::Concern

  included do
    before_action :logged_in_using_omniauth?
  end

  def logged_in_using_omniauth?
    @auth_payload, @auth_header = session[:userinfo]
    if @auth_payload.blank?
      return redirect_to "/"
    end

    @account = Account.from_token_payload(@auth_payload)
    if @account.blank?
      return redirect_to "/"
    end

    redirect_to controller: :character_detail, action: :new unless session[:chara_id]
  end

  def current_user
    @account
  end

  def current_character
    ::GameCharacter::Repository.find_player(current_character_raw_id)
  end

  # @return Character
  def current_character_record
    return Character.find(session[:chara_id]) if session[:chara_id]

    Character.find_by(account_id: @account.id)
  end

  def current_character_raw_id
    session[:chara_id]
  end
end
