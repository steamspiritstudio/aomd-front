class CommunicationLogsController < ApplicationController
  include Secured
  before_action :set_communication_log, only: %i[show edit update destroy]

  # GET /communication_logs
  # GET /communication_logs.json
  # 一覧
  def index
    #  @communication_logs = CommunicationLog.all
    # ↓レンダリング命令
    render "main_log_page"
  end
end
