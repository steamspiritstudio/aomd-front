class TradeNpcController < ApplicationController
  include Secured
  before_action :trade_npc, only: %i[show edit update destroy]

  # GET /trade_npc
  # GET /trade_npc.json
  # 一覧
  def index
    #  @trade_npc = trade_npc.all
    # ↓レンダリング命令
    render "index"
  end
end
