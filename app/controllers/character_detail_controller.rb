class CharacterDetailController < ApplicationController
  include Secured
  skip_before_action :logged_in_using_omniauth?, only: %i[new new_regist show index]
  protect_from_forgery except: %i[current edit]

  def current
    @character = current_character_record
    @ship = GameShip::Repository.find_or_new(@character.id)
    @items = ItemRepository.get_items(@character.id)
  end

  def index
    @characters = Character.all
  end

  def show
    @character = Character.find(params["id"])
    @ship = GameShip::Repository.find(@character.ship_id)
    target_turn = GameHelper.current_turn - 1
    @plan = PersonalPlan.where({ character_id: @character.id, turn: target_turn }).first
  end

  def new
    @form_model = ::NewCharacterCreateFormModel.new
  end

  def new_regist
    @auth_payload, @auth_header = session[:userinfo]
    if @auth_payload.blank?
      return redirect_to "/"
    end
    @account = Account.from_token_payload(@auth_payload)
    new_model = ::NewCharacterCreateFormModel.new(params.permit(:name, :portrait, :personalityFree, :ship_name))
    character_id = new_model.save(@account)
    if character_id === false
      @form_model = new_model
      return "character_detail/new"
    end
    session[:chara_id] = character_id
    redirect_to action: "current"
  end

  def edit
    record = permit_params
    record["id"] = current_character_raw_id
    record["personality"] = JSON.generate(
      {
        "free" => params[:character][:personalityFree] || ""
      },
    )
    @character = current_character_record
    @character.update!(record)
    redirect_to action: "current"
  end

  private

    def permit_params
      params.require(:character).permit(:name, :portrait)
    end
end
