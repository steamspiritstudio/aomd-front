class ShipyardController < ApplicationController
  include Secured

  def index
    character = current_character
    # TODO: Repositoryを使おう
    @ship_record = ::Ship.find_by(:id => character.character_id)
    if @ship_record == nil
      @ship_record = ::Ship.new(:id => character.character_id)
      # TODO: 初期船テンプレートをどこかで定義する
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 7, 1)
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 10, 1)
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 14, 1)
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 26, 1)
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 31, 1)
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 48, 1)
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 59, 1)
      @ship_record.ship_plans << ShipPlan.new_ship_parts(character.character_id, 72, 1)
    end
    @ship = GameShip::Repository.find_by_character_id(character.character_id)

    @shipyard_sell_list = MasterShipParts.where.not(name: '').map { |parts| parts.to_shipyard_json }.to_json
    @ship_parts_list = @ship_record.ship_plans.map { |parts|
      parts.to_shipyard_json
    }.to_json
  end

  def edit
    ship = ::GameShip::Repository.find_by_character_id(current_character.character_id)
    # FIXME: 資金導入後は直接編集ではなく、購入、売却コマンドを発行して更新時に処理すべき
    # @type Array
    input_plan = JSON.parse(params["ship_plan"])

    # 売却処理
    sell_ship_parts_ids = input_plan.filter do |parts_info|
      parts_info["sell"] && parts_info["id"] != nil && !parts_info["id"].to_s.match('^\d+@')
    end.map do |parts_info|
      parts_info["id"].to_i
    end
    unless sell_ship_parts_ids.empty?
      ShipPlan.where({ ship_id: ship.id, id: sell_ship_parts_ids }).destroy_all
    end

    # 購入処理
    new_ship_parts = input_plan.filter { |parts_info|
      parts_info["id"] == nil || parts_info["id"].to_s.match('^\d+@')
    }.map do |parts_info|
      ShipPlan.new_ship_parts(ship.id, parts_info["ship_parts_id"], parts_info["power_priority"])
    end
    new_ship_parts.map { |parts_record| parts_record.save  }

    redirect_to action: "index"
  end
end
