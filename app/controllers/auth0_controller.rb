class Auth0Controller < ApplicationController
  include Secured
  skip_before_action :logged_in_using_omniauth?, only: %i[login callback]
  protect_from_forgery except: [:callback]

  def login; end

  def callback
    # This stores all the user information that came from Auth0
    # and the IdP

    @auth_payload = request.env["omniauth.auth"]
    session[:userinfo] = [:uid => @auth_payload['uid'], :info => @auth_payload['info']]
    # もし対応するキャラクターがいない場合
    @account = Account.from_token_payload(@auth_payload)
    character = Character.find_by(account_id: @account.id)
    unless character
      redirect_to controller: :character_detail, action: :new
      return
    end
    session[:chara_id] = character.id

    # Redirect to the URL you want after successful auth
    redirect_to controller: :character_detail, action: :current
  end

  def failure
    # show a failure page or redirect to an error page
    @error_msg = request.params["message"]
  end
end
