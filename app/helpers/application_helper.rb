module ApplicationHelper
  def link_to_add_fields(name, f, association, className = nil)
    # Takes an object (@person) and creates a new instance of its associated model (:addresses)
    new_object = f.object.send(association).klass.new

    # Saves the unique ID of the object into a variable.
    id = new_object.object_id

    # https://api.rubyonrails.org/ fields_for(record_name, record_object = nil, fields_options = {}, &block)
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render("#{association.to_s.singularize}_fields", f: builder)
    end

    className = if className.nil?
                  "add_fields uk-button uk-button-small uk-button-secondary"
                else
                  "add_fields #{className}"
                end

    # This renders a simple link, but passes information into `data` attributes.
    link_to(name, "#", class: className, data: { id: id, fields: fields.gsub("\n", "") })
  end

  def logging_in?
    session.key?(:userinfo)
  end

  def chara_registered?
    session.key?(:chara_id)
  end

  def aomd_javascript_include_tag(source, options = {})
    unless AoMD::Application.config.x.enable_cloudinary
      return javascript_include_tag(source, options)
    end
    javascript_include_tag(Cloudinary::Utils.cloudinary_url(source + '.js', type: 'asset'), options)
  end

  def aomd_stylesheet_link_tag(source, options = {})
    unless AoMD::Application.config.x.enable_cloudinary
      return stylesheet_link_tag(source, options)
    end
    stylesheet_link_tag(Cloudinary::Utils.cloudinary_url(source + '.css', type: 'asset'), options)
  end
end
