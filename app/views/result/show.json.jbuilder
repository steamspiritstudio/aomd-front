json.ship_result do |ship|
  ship.array! @ship_action_results do |result|
    json.extract! result, :id, :ship_id, :turn, :time
    json.from_place result.from_place(@sectors)
    body = result.body_json
    json.type body["type"]
    if body["place"]
      json.to_place result.to_place(@sectors)
    end
  end
end
