require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AoMD
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # バッチのパスを追加
    config.eager_load_paths += Dir["#{config.root}/app/domains/*"].find_all { |f| File.stat(f).directory? }
    config.eager_load_paths += Dir["#{config.root}/app/assets/processor"]
    config.eager_load_paths += %W(#{config.root}/app/models/master)

    # アセットパイプライン無効化
    config.assets.enabled = false

    # 言語ファイル設定
    config.i18n.load_path += Dir[Rails.root.join("config", "locales", "*.{rb,yml}").to_s]

    # アプリケーションが対応している言語のホワイトリスト(ja = 日本語, en = 英語)
    config.i18n.available_locales = %i[ja]

    # 上記の対応言語以外の言語が指定された場合、エラーとするかの設定
    config.i18n.enforce_available_locales = false

    # デフォルトの言語設定
    # config.i18n.default_locale = :en
    config.i18n.default_locale = :ja

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.time_zone = "Tokyo"
    config.hosts << "aomd.tennana.net"
    config.hosts << "aomd-dev.tennana.net"

    #### ゲーム独自設定のネームスペース
    # キャラクター立ち絵のアップロードサイズ
    config.x.chara_portrait_size = { width: 300, height: 400 }

    # 行動宣言の追加可能行数
    config.x.chara_plan_action_max_limit = 20

    # 船の初期位置
    config.x.start_place = {
      sector_id: 1,
      x: 10,
      y: 10
    }

    config.x.enable_cloudinary = false

    # Session Storage
    config.session_store(
      :redis_store,
      servers: ENV.fetch("REDIS_URL", "localhost"),
      key: "_aomd_session",
      expire_after: 30.days
    )

    config.generators do |g|
      g.assets false
      g.helper false
      g.skip_routes false
      g.test_framework false
    end
  end
end
