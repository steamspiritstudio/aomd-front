CarrierWave.configure do |config|
  unless AoMD::Application.config.x.enable_cloudinary
    config.storage = :file
    return
  end
  config.cache_dir = "#{Rails.root}/tmp/uploads"
end
