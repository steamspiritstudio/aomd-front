Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get "api/private" => "private#private"
  get "api/private-scoped" => "private#private_scoped"
  get "auth/auth0/callback" => "auth0#callback"
  get "auth/failure" => "auth0#failure"
  get "auth/logout" => "logout#logout"
  get "auth/" => "auth0#login"

  root(to: "auth0#login")

  # Communication Logs
  #                               ↓コントロール用ファイル名       ↓メソッド名
  get "d/communication_logs" => "communication_logs#index"

  # Character Result
  get "d/result/:id" => "result#show", constraints: { id: /\d+/ }
  get "d/result/index" => "result#index"

  # Character Detail
  post "d/new_regist" => "character_detail#new_regist"
  get "d/new" => "character_detail#new"
  get "d/current" => "character_detail#current"
  get "d/:id" => "character_detail#show", constraints: { id: /\d+/ }
  post "d/current/edit" => "character_detail#edit"
  get "d/list" => "character_detail#index"

  # Character Planning
  get "d/current/personal" => "personal_plan#index"
  post "d/current/personal/edit" => "personal_plan#edit"
  patch "d/current/personal/edit" => "personal_plan#edit"

  # Ship Detail
  get 'sd/index' => "ship_detail#index"
  get 'sd/:id' => "ship_detail#show", constraints: { id: /\d+/ }

  # Sector
  get "s/:id" => "sector#show", constraints: { id: /\d+/ }
  get "s" => "sector#index"

  # Item Deal
  get "items" => "item_deal#index"

  # Ship Design
  get "shipyard" => "shipyard#index"
  post "shipyard" => "shipyard#edit"

  # NPC Trade
  get "trade_npc" => "trade_npc#index"

  # static_pages
  get "credits" => "static_pages#credit"
end
