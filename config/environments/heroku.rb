Rails.application.configure do
  config.assets.compile = true
  config.cache_classes = true
  config.eager_load = false

  config.hosts << "tennana-aomd-dev.herokuapp.com"
  config.hosts << "aomd.tennana.net"

  config.x.enable_cloudinary = true
end
