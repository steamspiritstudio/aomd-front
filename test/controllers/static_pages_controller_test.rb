require "test_helper"

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get credit" do
    get static_pages_credit_url
    assert_response :success
  end
end
