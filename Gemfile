source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.0.0"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem "rails", "6.1.4.1"
# Use mysql as the database for Active Record
gem "mysql2"
# Use Puma as the app server
gem "puma"
gem 'sassc-rails'
gem "jsbundling-rails"
gem "cssbundling-rails"

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem "jbuilder"
# Use Redis adapter to run Action Cable in production
# gem 'redis'
# Use Active Model has_secure_password
# gem 'bcrypt'

# Use Active Storage variant
# gem 'image_processing

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "byebug", platforms: %i[mri mingw x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem "listen"
  gem "web-console"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "rubocop", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
  gem "spring"
  gem "spring-watcher-listen"
  gem 'guard'
  gem 'guard-livereload', require: false
  gem 'guard-bundler', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]

gem "dotenv-rails"
gem "jwt"
group :heroku do
  gem "pg"
  gem "rails_12factor"
end

gem "omniauth-auth0"
gem "omniauth-google-oauth2"
gem "omniauth-rails_csrf_protection"

gem "annotate"

# DB Gem
gem 'seed-fu', '~> 2.3'
gem 'active_hash'
gem 'redis-actionpack'

# Image Gem
gem 'cloudinary'
gem "carrierwave"
gem "mini_magick"

# rails-i18n
gem "rails-i18n"
