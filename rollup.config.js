import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import image from '@rollup/plugin-image';
import commonjs from "@rollup/plugin-commonjs";
import json from '@rollup/plugin-json';
import svelte from 'rollup-plugin-svelte';
import autoPreprocess from 'svelte-preprocess';
import postcss from "rollup-plugin-postcss";
import copy from "rollup-plugin-copy";

const plugins = [
    json({
        // for tree-shaking, properties will be declared as
        // variables, using either `var` or `const`
        preferConst: true, // Default: false
        // ignores indent and generates the smallest code
        compact: true, // Default: false
        // generate a named export for every property of the JSON object
        namedExports: false // Default: true
    }),
    image(),
    typescript({
        sourceMap: true,
        moduleResolution: 'node'
    }),
    svelte({
        preprocess: autoPreprocess({
            typescript: {
                compilerOptions: {
                    sourcemap: true
                },
                tsconfigDirectory: __dirname,
            },
            scss: {
                includePaths: ['app/assets/'],
                output: false
            },
            postcss: false
        }),
        // You can pass any of the Svelte compiler options
        compilerOptions: {
            // ensure that extra attributes are added to head
            // elements for hydration (used with generate: 'ssr')
            hydratable: true,

            // You can optionally set 'customElement' to 'true' to compile
            // your components to custom elements (aka web elements)
            customElement: false,
            css: true,
            sourcemap: true
        }
    }),
    commonjs(),
    resolve({
        browser: true,
        extensions: ['.js', '.jsx', '.mjs', '.ts', '.tsx'],
        dedupe: ['svelte', 'svelte/transition', 'svelte/internal']
    }),
    copy({
        copyOnce: true,
        targets: [{src: 'node_modules/sss-telegraph/public/fonts/', dest: 'app/assets/builds/'}]
    })
];

const onwarn = (warning, defaultHandler) => {
    if (warning.code === 'THIS_IS_UNDEFINED') return;
    defaultHandler(warning)
};

function createSetting(inputRelativePath) {
    const outputJsFilePath = inputRelativePath.replace('app/javascript/', 'app/assets/builds/');
    return {
        input: inputRelativePath,
        output: {
            file: outputJsFilePath,
            format: "es",
            inlineDynamicImports: true,
            sourcemap: true
        },
        watch: {
            chokidar: {
                paths: "app/javascript/**",
                usePolling: true
            }
        },
        plugins: plugins.concat([
            postcss({
                use: "sass",
                config: {path : __dirname + "/postcss.config.js"},
                to: outputJsFilePath.replace(".js", ".css")
            })
        ]),
        onwarn
    };
}

export default [
    createSetting("app/javascript/application.js"),
    createSetting("app/javascript/shipyard/index.js"),
    createSetting("app/javascript/personal_plan/index.js"),
    createSetting("app/javascript/sector/show.js"),
    createSetting("app/javascript/result/show.js")
];