class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    # アカウント情報
    create_table :accounts do |t|
      t.string :name,       comment: "ユーザー名"
      t.string :auth0_uid,  comment: "認証情報"

      t.timestamps  null: false
    end
  end
end