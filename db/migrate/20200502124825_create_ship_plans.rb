class CreateShipPlans < ActiveRecord::Migration[6.0]
  def change
    create_table :ship_plans do |t|
      # 船のパーツ情報
      t.bigint  :part_id,   unsigned: true, comment: "パーツID"
      t.bigint  :ship_id,   unsigned: true, comment: "船ID"
      t.integer :priority,  unsigned: true, comment: "エネルギー優先度"
      t.integer :hp,                        comment: "HP"
      t.integer :hp_max,                    comment: "最大HP(保守)"
      t.text :tags, default: "{}", comment: "タグ"

      t.timestamps
    end
  end
end
