class CreateShips < ActiveRecord::Migration[6.0]
  def change
    # 船データ
    create_table :ships, id: false do |t|
      t.primary_key :id,                                            comment: "管理ID"
      t.string :callsign, comment: "コールサイン"
      t.string :name, comment: "船名"
      t.bigint :captain_id, unsigned: true, comment: "船長ID"
      t.bigint :place_sector_id, null: false, comment: "現在位置_セクターID"
      t.integer :coordinate_x, null: false, comment: "現在位置_X座標"
      t.integer :coordinate_y, null: false, comment: "現在位置_Y座標"
      t.bigint :place_station_id, unsigned: true, comment: "現在位置_ステーションID"
      t.text :tags, default: '', comment: "タグ情報"
      t.integer :shelds, comment: "シールド残量"
      t.bigint :money, comment: "共有所持金"
      t.text :cargo, comment: "船倉状況{[アイテムID],[量]}"
      t.text :items, comment: "共有アイテム{[アイテムID],[数]}"
      t.text :free_txt, comment: "自由記入欄"
      t.text :picture, comment: "画像"
      t.timestamps
    end
  end
end
