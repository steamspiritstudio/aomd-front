class CreateCharacterActionResults < ActiveRecord::Migration[6.1]
  def change
    create_table :character_action_results, id: false do |t|
      t.string :id, primary_key: true, comment: 'ID(メッセージ用)'
      t.integer :turn, unsigned: true, null: false, comment: '発生ターン'
      t.integer :time, unsigned: true, null: false, comment: '発生時刻'
      t.bigint :character_id, unsigned: true, null: false, comment: '行動者'
      t.bigint :sector_id, unsigned: true, null: false, comment: '発生セクター'
      t.bigint :ship_id, unsigned: true, comment: '発生船'
      t.integer :ship_x, unsigned: true, comment: '船のX座標'
      t.integer :ship_y, unsigned: true, comment: '船のY座標'
      t.bigint :station_id, unsigned: true, comment: 'ステーション着陸状態'
      t.text :body_json, null: false, comment: '行動詳細'

      t.timestamps
    end
    # 場所別取得用インデックス
    add_index :character_action_results, [:turn, :sector_id], name: 'turn_by_sector_index'
    add_index :character_action_results, [:turn, :character_id], name: 'turn_by_character_index'
    add_index :character_action_results, [:turn, :ship_id], name: 'turn_by_ship_index'
  end
end
