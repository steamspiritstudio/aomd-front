class CreateSectorOfSpaces < ActiveRecord::Migration[6.0]
  def change
    # セクター情報
    create_table :sector_of_spaces do |t|
      t.string :name,  null: false, comment: "セクター名"
    end
  end
end
