class CreateCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :characters, id: false do |t|
      # キャラクター情報
      t.primary_key :id, comment: "管理ID"
      t.bigint :account_id, unsigned: true, comment: "アカウントID"
      t.string :callsign, comment: "コールサイン"
      t.string :name, comment: "名前"
      t.integer :register_turn, null: false, unsigned: true, comment: "作成ターン"
      t.string :portrait, comment: "画像(立ち絵)"
      t.text :personality, comment: "自由記入欄"
      t.text :icon_ids, comment: "会話用アイコン"
      t.integer :place_id_in_ship, unsigned: true, comment: "乗船している船ID"
      t.integer :helth, comment: "生命力"
      t.integer :sp, comment: "残SP"
      t.text :tags, default: "{}", comment: "タグ"
      t.integer :money, unsigned: true, comment: "所持金"
      t.text :items, comment: "所持アイテム"

      t.timestamps
    end
  end
end
