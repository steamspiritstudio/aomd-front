class CreateWorlds < ActiveRecord::Migration[6.1]
  def change
    create_table :worlds do |t|
      t.integer :turn, limit: 1, unsigned: true
      t.integer :phase, limit: 1, unsigned: true

      t.timestamps
    end
  end
end
