class CreateShipFlightSchedules < ActiveRecord::Migration[6.1]
  def change
    create_table :ship_flight_schedules do |t|
      t.integer :ship_id, unsigned: true, comment: '船ID', null: false
      t.integer :to_sector_id, unsigned: true, comment: '移動先宙域ID'
      t.integer :to_station_id, unsigned: true, comment: '移動先ステーションID'
      t.integer :coordinate_x, default: 0, unsigned: true, comment: '移動先X'
      t.integer :coordinate_y, default: 0, unsigned: true, comment: '移動先Y'
      t.integer :wait_ap, default: 0, unsigned: true, null: false, comment: '滞在時間(0のみ特殊)'
      t.integer :register_account_id, unsigned: true, null: false, comment: '登録者ID'
      t.integer :register_character_id, unsigned: true, null: false, comment: '登録者キャラクターID'

      t.timestamps
    end
    add_index :ship_flight_schedules, [:ship_id, :register_character_id], name: 'ship_index'
  end
end
