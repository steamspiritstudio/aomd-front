class CreateSectorFairways < ActiveRecord::Migration[6.0]
  def change
    # 航路情報
    create_table :sector_fairways do |t|
      t.integer :sector_id_alpha,   unsigned: true, null: false,                                      comment: "α座標セクターID"
      t.integer :move_range_alpha,                  null: false, default: 1, after: :sector_id_alpha, comment: "α座標移動ベクトル"
      t.integer :central_coordinate_alpha_y,        null: false,             after: :sector_id_alpha, comment: "α座標X座標"
      t.integer :central_coordinate_alpha_x,        null: false,             after: :sector_id_alpha, comment: "α座標Y座標"

      t.integer :sector_id_beta,    unsigned: true, null: false,                                      comment: "β座標セクターID"
      t.integer :move_range_beta,                   null: false, default: 1, after: :sector_id_alpha, comment: "β座標移動ベクトル"
      t.integer :central_coordinate_beta_y,         null: false,             after: :sector_id_alpha, comment: "β座標X座標"
      t.integer :central_coordinate_beta_x,         null: false,             after: :sector_id_alpha, comment: "β座標Y座標"
    end
  end
end
