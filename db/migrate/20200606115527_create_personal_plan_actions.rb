class CreatePersonalPlanActions < ActiveRecord::Migration[6.0]
  def change
    # 個人計画結果
    create_table :personal_plan_actions, id: false do |t|
      t.integer :character_id, unsigned: true, comment: "キャラクターID"
      t.integer :turn, unsigned: true, comment: "ターン"
      t.integer :order, unsigned: true, comment: "順番"
      t.integer :use_ap, unsigned: true, comment: "使用AP"
      t.integer :from_time, unsigned: true, comment: "開始時間"
      t.string :action_type, limit: 32, comment: "行動Type"
      t.integer :target_id, unsigned: true, comment: "対象ID"
      t.text :param_json, comment: "補助パラメーター"

      t.timestamps
    end
    add_index :personal_plan_actions, %i[turn character_id order], unique: true
  end
end
