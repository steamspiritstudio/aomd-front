class CreateMasterCorp < ActiveRecord::Migration[6.0]
  def change
    # 企業データ
    create_table :master_corp do |t|
      t.string :name, comment: "名前"
      t.string :short_name, comment: "略称"
      t.string :icon, comment: "企業ロゴ"
    end
  end
end