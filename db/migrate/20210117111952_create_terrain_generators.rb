class CreateTerrainGenerators < ActiveRecord::Migration[6.0]
  def change
    # 地形生成データ
    create_table :terrain_generators do |t|
      t.integer :sector_id,             unsigned: true, null: false,            comment: "セクターID"
      t.integer :central_coordinate_x,                  null: false,            comment: "座標X"
      t.integer :central_coordinate_y,                  null: false,            comment: "座標Y"
      t.integer :generate_type,                                     default: 1, comment: "生成タイプ"
      t.integer :range,                                 null: false,            comment: "範囲"
      t.integer :radial_rays,                                       default: 0, comment: "放射線"
      t.integer :gas,                                               default: 0, comment: "ガス"
      t.integer :small_body,                                        default: 0, comment: "小惑星"
      t.integer :gravitation,                                       default: 0, comment: "重力"
    end
  end
end
