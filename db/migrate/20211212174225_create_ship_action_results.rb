class CreateShipActionResults < ActiveRecord::Migration[6.1]
  def change
    create_table :ship_action_results, id: false do |t|
      t.string :id, primary: true, null: false
      t.bigint :ship_id, unsigned: true, null: false
      t.integer :turn, unsigned: true, null: false
      t.integer :time, unsigned: true, null: false
      t.string :ship_name, null: false
      t.integer :supply_power, unsigned: true, null: false
      t.integer :surplus_power, unsigned: true, null: false
      t.bigint :sector_id, unsigned: true, null: false
      t.integer :x, unsigned: true, null: false
      t.integer :y, unsigned: true, null: false
      t.bigint :station_id, unsigned: true, null: true
      t.text :body_json, null: false
      t.timestamps
    end
    add_index :ship_action_results, [:turn, :sector_id, :time], name: 'turn_in_sector_index'
    add_index :ship_action_results, [:turn, :ship_id, :time], name: 'turn_for_ship_index'
  end
end
