class CreateMasterItems < ActiveRecord::Migration[6.0]
  def change
    create_table :master_items do |t|
      # アイテムテーブルマスタ
      t.string  :name,                                        comment: "アイテム名"
      t.text :base_tag, default: "{}", null: false, comment: "タグ情報"
      t.integer :max_use_action,  unsigned: true,             comment: "最大使用量"
      t.integer :cargo_size,      unsigned: true,             comment: "容量占有量"
      t.boolean :tradable,                                    comment: "取引可能か"
    end
  end
end
