class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      # メッセージ
      t.integer :message_type,  unsigned: true, comment: "通信タイプ{1:低通信,2:高通信}"
      t.integer :turn,          unsigned: true, comment: "受信ターン"
      t.bigint  :from_id,       unsigned: true, comment: "送信元船ID"
      t.integer :has_type,                      comment: "送信先タイプ{1:船宛,2:セクター宛}"
      t.bigint  :has_id,        unsigned: true, comment: "発信先ID"
      t.text    :body,                          comment: "通信本体"

      t.timestamps
    end
  end
end
