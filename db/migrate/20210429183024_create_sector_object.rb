class CreateSectorObject < ActiveRecord::Migration[6.0]
  def change
    # セクター生成物
    create_table :sector_object do |t|
      t.text    :name,                                                          comment: "名前"
      t.integer :type,                                                          comment: "オブジェクトの種類"
      t.text    :discovery_name,                                                comment: "発見者の名前"
      t.integer :sector_id,             unsigned: true, null: false,            comment: "セクターID"
      t.integer :central_coordinate_x,                  null: false,            comment: "座標X"
      t.integer :central_coordinate_y,                  null: false,            comment: "座標Y"
    end
  end
end