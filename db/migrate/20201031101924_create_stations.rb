class CreateStations < ActiveRecord::Migration[6.0]
  def change
    create_table :stations do |t|
      # ステーション名
      t.string :name, comment: "名前"
      t.bigint :sector_id, null: false, comment: "セクターID"
      t.integer :coordinate_x, null: false, comment: "座標X"
      t.integer :coordinate_y, null: false, comment: "座標Y"
    end
    add_foreign_key :stations, :sector_of_spaces, column: :sector_id
  end
end
