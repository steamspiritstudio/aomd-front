class CreateTransactionPlans < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_plans do |t|
      # 取引情報
      t.integer :type,      unsigned: true, comment: ""
      t.integer :from_type, unsigned: true, comment: ""
      t.integer :from_id,   unsigned: true, comment: ""
      t.text :from_body, comment: ""
      t.integer :to_type,   unsigned: true, comment: ""
      t.integer :to_id,     unsigned: true, comment: ""
      t.text :to_body, comment: ""

      t.timestamps
    end
  end
end
