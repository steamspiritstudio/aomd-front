class CreatePersonalPlans < ActiveRecord::Migration[6.0]
  def change
    # 個人計画
    create_table :personal_plans do |t|
      t.integer :character_id,        unsigned: true,               comment: "キャラクターID"
      t.integer :turn,                unsigned: true,               comment: "ターン数"
      t.text    :daily,                                             comment: "個人日誌"
      t.timestamps
    end
    add_index :personal_plans, :character_id
    add_index :personal_plans, %i[turn character_id], unique: true
  end
end
