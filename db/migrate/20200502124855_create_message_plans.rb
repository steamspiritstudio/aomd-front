class CreateMessagePlans < ActiveRecord::Migration[6.0]
  def change
    create_table :message_plans do |t|
      # メッセージプラン
      t.integer :message_type,  unsigned: true, comment: "通信タイプ{1:低通信,2:高通信}"
      t.integer :turn,          unsigned: true, comment: "送信ターン"
      t.bigint  :place_id,      unsigned: true, comment: "発信セクターID"
      t.bigint  :from_id,       unsigned: true, comment: "発信船ID"
      t.integer :to_type,                       comment: "送信先タイプ{1:船宛,2:セクター宛}"
      t.bigint  :to_id,         unsigned: true, comment: "送信先ID"
      t.text    :to_character_name,             comment: "発信先名前"
      t.text    :body,                          comment: "通信本体"
      t.integer :sended_id,     unsigned: true, comment: "送信ターン"

      t.timestamps
    end
  end
end
