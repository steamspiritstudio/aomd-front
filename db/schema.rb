# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_05_29_071139) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "name", comment: "ユーザー名"
    t.string "auth0_uid", comment: "認証情報"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "character_action_results", id: { type: :string, comment: "ID(メッセージ用)" }, force: :cascade do |t|
    t.integer "turn", null: false, comment: "発生ターン"
    t.integer "time", null: false, comment: "発生時刻"
    t.bigint "character_id", null: false, comment: "行動者"
    t.bigint "sector_id", null: false, comment: "発生セクター"
    t.bigint "ship_id", comment: "発生船"
    t.integer "ship_x", comment: "船のX座標"
    t.integer "ship_y", comment: "船のY座標"
    t.bigint "station_id", comment: "ステーション着陸状態"
    t.text "body_json", null: false, comment: "行動詳細"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["turn", "character_id"], name: "turn_by_character_index"
    t.index ["turn", "sector_id"], name: "turn_by_sector_index"
    t.index ["turn", "ship_id"], name: "turn_by_ship_index"
  end

  create_table "characters", id: { comment: "管理ID" }, force: :cascade do |t|
    t.bigint "account_id", comment: "アカウントID"
    t.string "callsign", comment: "コールサイン"
    t.string "name", comment: "名前"
    t.integer "register_turn", null: false, comment: "作成ターン"
    t.string "portrait", comment: "画像(立ち絵)"
    t.text "personality", comment: "自由記入欄"
    t.text "icon_ids", comment: "会話用アイコン"
    t.integer "place_id_in_ship", comment: "乗船している船ID"
    t.integer "helth", comment: "生命力"
    t.integer "sp", comment: "残SP"
    t.text "tags", default: "{}", comment: "タグ"
    t.integer "money", comment: "所持金"
    t.text "items", comment: "所持アイテム"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "master_corp", force: :cascade do |t|
    t.string "name", comment: "名前"
    t.string "short_name", comment: "略称"
    t.string "icon", comment: "企業ロゴ"
  end

  create_table "master_items", force: :cascade do |t|
    t.string "name", comment: "アイテム名"
    t.text "base_tag", default: "{}", null: false, comment: "タグ情報"
    t.integer "max_use_action", comment: "最大使用量"
    t.integer "cargo_size", comment: "容量占有量"
    t.boolean "tradable", comment: "取引可能か"
  end

  create_table "message_plans", force: :cascade do |t|
    t.integer "message_type", comment: "通信タイプ{1:低通信,2:高通信}"
    t.integer "turn", comment: "送信ターン"
    t.bigint "place_id", comment: "発信セクターID"
    t.bigint "from_id", comment: "発信船ID"
    t.integer "to_type", comment: "送信先タイプ{1:船宛,2:セクター宛}"
    t.bigint "to_id", comment: "送信先ID"
    t.text "to_character_name", comment: "発信先名前"
    t.text "body", comment: "通信本体"
    t.integer "sended_id", comment: "送信ターン"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "messages", force: :cascade do |t|
    t.integer "message_type", comment: "通信タイプ{1:低通信,2:高通信}"
    t.integer "turn", comment: "受信ターン"
    t.bigint "from_id", comment: "送信元船ID"
    t.integer "has_type", comment: "送信先タイプ{1:船宛,2:セクター宛}"
    t.bigint "has_id", comment: "発信先ID"
    t.text "body", comment: "通信本体"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "personal_plan_actions", id: false, force: :cascade do |t|
    t.integer "character_id", comment: "キャラクターID"
    t.integer "turn", comment: "ターン"
    t.integer "order", comment: "順番"
    t.integer "use_ap", comment: "使用AP"
    t.integer "from_time", comment: "開始時間"
    t.string "action_type", limit: 32, comment: "行動Type"
    t.integer "target_id", comment: "対象ID"
    t.text "param_json", comment: "補助パラメーター"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["turn", "character_id", "order"], name: "index_personal_plan_actions_on_turn_and_character_id_and_order", unique: true
  end

  create_table "personal_plans", force: :cascade do |t|
    t.integer "character_id", comment: "キャラクターID"
    t.integer "turn", comment: "ターン数"
    t.text "daily", comment: "個人日誌"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["character_id"], name: "index_personal_plans_on_character_id"
    t.index ["turn", "character_id"], name: "index_personal_plans_on_turn_and_character_id", unique: true
  end

  create_table "sector_fairways", force: :cascade do |t|
    t.integer "sector_id_alpha", null: false, comment: "α座標セクターID"
    t.integer "move_range_alpha", default: 1, null: false, comment: "α座標移動ベクトル"
    t.integer "central_coordinate_alpha_y", null: false, comment: "α座標X座標"
    t.integer "central_coordinate_alpha_x", null: false, comment: "α座標Y座標"
    t.integer "sector_id_beta", null: false, comment: "β座標セクターID"
    t.integer "move_range_beta", default: 1, null: false, comment: "β座標移動ベクトル"
    t.integer "central_coordinate_beta_y", null: false, comment: "β座標X座標"
    t.integer "central_coordinate_beta_x", null: false, comment: "β座標Y座標"
  end

  create_table "sector_object", force: :cascade do |t|
    t.text "name", comment: "名前"
    t.integer "type", comment: "オブジェクトの種類"
    t.text "discovery_name", comment: "発見者の名前"
    t.integer "sector_id", null: false, comment: "セクターID"
    t.integer "central_coordinate_x", null: false, comment: "座標X"
    t.integer "central_coordinate_y", null: false, comment: "座標Y"
  end

  create_table "sector_of_spaces", force: :cascade do |t|
    t.string "name", null: false, comment: "セクター名"
  end

  create_table "ship_action_results", id: false, force: :cascade do |t|
    t.string "id", null: false
    t.bigint "ship_id", null: false
    t.integer "turn", null: false
    t.integer "time", null: false
    t.string "ship_name", null: false
    t.integer "supply_power", null: false
    t.integer "surplus_power", null: false
    t.bigint "sector_id", null: false
    t.integer "x", null: false
    t.integer "y", null: false
    t.bigint "station_id"
    t.text "body_json", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["turn", "sector_id", "time"], name: "turn_in_sector_index"
    t.index ["turn", "ship_id", "time"], name: "turn_for_ship_index"
  end

  create_table "ship_flight_schedules", force: :cascade do |t|
    t.integer "ship_id", null: false, comment: "船ID"
    t.integer "to_sector_id", comment: "移動先宙域ID"
    t.integer "to_station_id", comment: "移動先ステーションID"
    t.integer "coordinate_x", default: 0, comment: "移動先X"
    t.integer "coordinate_y", default: 0, comment: "移動先Y"
    t.integer "wait_ap", default: 0, null: false, comment: "滞在時間(0のみ特殊)"
    t.integer "register_account_id", null: false, comment: "登録者ID"
    t.integer "register_character_id", null: false, comment: "登録者キャラクターID"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ship_id", "register_character_id"], name: "ship_index"
  end

  create_table "ship_plans", force: :cascade do |t|
    t.bigint "part_id", comment: "パーツID"
    t.bigint "ship_id", comment: "船ID"
    t.integer "priority", comment: "エネルギー優先度"
    t.integer "hp", comment: "HP"
    t.integer "hp_max", comment: "最大HP(保守)"
    t.text "tags", default: "{}", comment: "タグ"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ships", id: { comment: "管理ID" }, force: :cascade do |t|
    t.string "callsign", comment: "コールサイン"
    t.string "name", comment: "船名"
    t.bigint "captain_id", comment: "船長ID"
    t.bigint "place_sector_id", null: false, comment: "現在位置_セクターID"
    t.integer "coordinate_x", null: false, comment: "現在位置_X座標"
    t.integer "coordinate_y", null: false, comment: "現在位置_Y座標"
    t.bigint "place_station_id", comment: "現在位置_ステーションID"
    t.text "tags", default: "", comment: "タグ情報"
    t.integer "shelds", comment: "シールド残量"
    t.bigint "money", comment: "共有所持金"
    t.text "cargo", comment: "船倉状況{[アイテムID],[量]}"
    t.text "items", comment: "共有アイテム{[アイテムID],[数]}"
    t.text "free_txt", comment: "自由記入欄"
    t.text "picture", comment: "画像"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "stations", force: :cascade do |t|
    t.string "name", comment: "名前"
    t.bigint "sector_id", null: false, comment: "セクターID"
    t.integer "coordinate_x", null: false, comment: "座標X"
    t.integer "coordinate_y", null: false, comment: "座標Y"
  end

  create_table "terrain_generators", force: :cascade do |t|
    t.integer "sector_id", null: false, comment: "セクターID"
    t.integer "central_coordinate_x", null: false, comment: "座標X"
    t.integer "central_coordinate_y", null: false, comment: "座標Y"
    t.integer "generate_type", default: 1, comment: "生成タイプ"
    t.integer "range", null: false, comment: "範囲"
    t.integer "radial_rays", default: 0, comment: "放射線"
    t.integer "gas", default: 0, comment: "ガス"
    t.integer "small_body", default: 0, comment: "小惑星"
    t.integer "gravitation", default: 0, comment: "重力"
  end

  create_table "transaction_plans", force: :cascade do |t|
    t.integer "type"
    t.integer "from_type"
    t.integer "from_id"
    t.text "from_body"
    t.integer "to_type"
    t.integer "to_id"
    t.text "to_body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "worlds", force: :cascade do |t|
    t.integer "turn", limit: 2
    t.integer "phase", limit: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "stations", "sector_of_spaces", column: "sector_id"
end
