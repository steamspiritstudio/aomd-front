SectorOfSpace.seed_once(:id) do |s|
  s.id = 1
  s.name = "地球軌道"
end

SectorOfSpace.seed_once(:id) do |s|
  s.id = 2
  s.name = "地球-月軌道間加速航路"
end

SectorOfSpace.seed_once(:id) do |s|
  s.id = 3
  s.name = "月機動"
end

SectorOfSpace.seed_once(:id) do |s|
  s.id = 4
  s.name = "月-火星間加速航路"
end

SectorOfSpace.seed_once(:id) do |s|
  s.id = 5
  s.name = "火星軌道"
end

SectorOfSpace.seed_once(:id) do |s|
  s.id = 6
  s.name = "小惑星帯(メインベルト)"
end

SectorFairway.seed_once(:id) do |s|
  s.id = 1
  s.sector_id_alpha = 1
  s.sector_id_beta = 2

  s.move_range_alpha = 3
  s.central_coordinate_alpha_x = 19
  s.central_coordinate_alpha_y = 10
  s.move_range_beta = 3
  s.central_coordinate_beta_x = 2
  s.central_coordinate_beta_y = 2
end

SectorFairway.seed_once(:id) do |s|
  s.id = 2
  s.sector_id_alpha = 2
  s.sector_id_beta = 3

  s.move_range_alpha = 3
  s.central_coordinate_alpha_x = 18
  s.central_coordinate_alpha_y = 15
  s.move_range_beta = 3
  s.central_coordinate_beta_x = 2
  s.central_coordinate_beta_y = 18
end

SectorFairway.seed_once(:id) do |s|
  s.id = 3
  s.sector_id_alpha = 3
  s.sector_id_beta = 4

  s.move_range_alpha = 3
  s.central_coordinate_alpha_x = 18
  s.central_coordinate_alpha_y = 18
  s.move_range_beta = 3
  s.central_coordinate_beta_x = 2
  s.central_coordinate_beta_y = 2
end

SectorFairway.seed_once(:id) do |s|
  s.id = 4
  s.sector_id_alpha = 4
  s.sector_id_beta = 5

  s.move_range_alpha = 3
  s.central_coordinate_alpha_x = 18
  s.central_coordinate_alpha_y = 18
  s.move_range_beta = 3
  s.central_coordinate_beta_x = 1
  s.central_coordinate_beta_y = 10
end

SectorFairway.seed_once(:id) do |s|
  s.id = 5
  s.sector_id_alpha = 5
  s.sector_id_beta = 6

  s.move_range_alpha = 3
  s.central_coordinate_alpha_x = 20
  s.central_coordinate_alpha_y = 20
  s.move_range_beta = 3
  s.central_coordinate_beta_x = 1
  s.central_coordinate_beta_y = 1
end

Station.seed_once(:id) do |s|
  s.id = 1
  s.name = "地球駅舎"
  s.sector_id = 1
  s.coordinate_x = 10
  s.coordinate_y = 10
end

Station.seed_once(:id) do |s|
  s.id = 2
  s.name = "月面基地"
  s.sector_id = 3
  s.coordinate_x = 10
  s.coordinate_y = 10
end
=begin
Station.seed_once(:id) do |s|
  s.id = 3
  s.name = "地球"
  s.sector_id = 1
  s.coordinate_x = 2
  s.coordinate_y = 10
end

Station.seed_once(:id) do |s|
  s.id = 4
  s.name = "月"
  s.sector_id = 3
  s.coordinate_x = 10
  s.coordinate_y = 1
end

Station.seed_once(:id) do |s|
  s.id = 5
  s.name = "エロス"
  s.sector_id = 4
  s.coordinate_x = 10
  s.coordinate_y = 13
end

Station.seed_once(:id) do |s|
  s.id = 6
  s.name = "火星"
  s.sector_id = 5
  s.coordinate_x = 10
  s.coordinate_y = 1
end
=end
Station.seed_once(:id) do |s|
  s.id = 7
  s.name = "火星駅舎"
  s.sector_id = 5
  s.coordinate_x = 10
  s.coordinate_y = 5
end
=begin
Station.seed_once(:id) do |s|
  s.id = 8
  s.name = "ファボス"
  s.sector_id = 5
  s.coordinate_x = 10
  s.coordinate_y = 10
end

Station.seed_once(:id) do |s|
  s.id = 9
  s.name = "デイモス"
  s.sector_id = 5
  s.coordinate_x = 5
  s.coordinate_y = 20
end

Station.seed_once(:id) do |s|
  s.id = 10
  s.name = "ケレス"
  s.sector_id = 6
  s.coordinate_x = 15
  s.coordinate_y = 15
end

Station.seed_once(:id) do |s|
  s.id = 11
  s.name = "リュウグウ"
  s.sector_id = 6
  s.coordinate_x = 20
  s.coordinate_y = 15
end

Station.seed_once(:id) do |s|
  s.id = 12
  s.name = "イトカワ"
  s.sector_id = 6
  s.coordinate_x = 10
  s.coordinate_y = 5
end
=end
Station.seed_once(:id) do |s|
  s.id = 13
  s.name = "小惑星帯前哨基地"
  s.sector_id = 6
  s.coordinate_x = 14
  s.coordinate_y = 10
end
