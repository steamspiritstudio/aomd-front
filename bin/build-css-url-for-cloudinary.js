const replace_css_url = require('replace-css-url')
const fs = require('fs');
const path = require('path');
const minify = require('csso').minify

const staticSetting = fs.readFileSync(path.join(__dirname, "/../.cloudinary.static"), {encoding: 'utf8'});
const css = fs.readFileSync(path.join(__dirname, "/../app/assets/stylesheets/game-icons.css"), {encoding: 'utf8'});

fs.writeFileSync(path.join(__dirname, "/../app/assets/builds/game-icons.css"), minify(replace_css_url(
    css,
    function (filePath) {
        const filename = path.basename(filePath);
        const index = staticSetting.indexOf(filename);
        if (index === -1) {
            return filePath;
        }
        const cloudinaryUrl = "./" + /\t([^\t]+)/.exec(staticSetting.substring(index))[1] + path.extname(filePath);
        console.log(filePath, "=>", cloudinaryUrl);
        return cloudinaryUrl;
    }
)).css, {encoding: "utf8"})