const postcssUrl = require("postcss-url");
module.exports = {
  extract: true,
  plugins: [
    postcssUrl({
      url: "inline", // enable inline assets using base64 encoding
      maxSize: 100, // maximum file size to inline (in kilobytes)
      fallback: "copy", // fallback method to use if max size is exceeded
    }),
//    require('postcss-flexbugs-fixes'),
//     require('postcss-preset-env')({
//       autoprefixer: {
//         flexbox: 'no-2009'
//       },
//       stage: 3
//     })
  ]
}
